﻿namespace InventoryIT.Core.Constants
{
    public class InventoryITConstants
    {
        public const string Amortisation = "AmortisationController/GetByAmortisationIdAsync";
        public const string Success = "Запрос обработан";
        public const string Failed = "Ошибка при обработке запроса";
    }
}