﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions
{
    public interface IGenericRepository<T> where T: BaseEntity
    {
        Task<InventoryITActionResult<IEnumerable<T>>> GetAllAsync();

        Task<InventoryITActionResult<T>> GetEntityByIdAsync(int id);

        Task<InventoryITActionResult> AddAsync(T entity);

        Task<InventoryITActionResult> UpdateAsync(T entity);

        Task<InventoryITActionResult> DeleteAsync(T entity);
    }
}
