﻿using InventoryIT.Core.Models.SpecificationBlankModels;
using InventoryIT.Core.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Services
{
    public interface ISpecificationBlankService : IBaseService<SpecificationBlankDTO>
    {
    }
}
