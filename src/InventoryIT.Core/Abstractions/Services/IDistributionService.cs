﻿using InventoryIT.Core.Models.DistributionModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Services
{
    public interface IDistributionService : IBaseService<DistributionDTO>
    {
    }
}
