﻿using InventoryIT.Core.Models.SpecificationModels;
using InventoryIT.Core.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Services
{
    public interface ISpecificationService : IBaseService<SpecificationDTO>
    {
    }
}
