﻿using InventoryIT.Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Services
{
    public interface IProductService : IBaseService<ProductDTO>
    {
    }
}
