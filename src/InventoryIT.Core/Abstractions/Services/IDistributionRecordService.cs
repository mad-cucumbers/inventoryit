﻿using InventoryIT.Core.Models.DistributionRecordModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Services
{
    public interface IDistributionRecordService : IBaseService<DistributionRecordDTO>
    {
    }
}
