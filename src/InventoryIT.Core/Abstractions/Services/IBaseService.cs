﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Services
{
    public interface IBaseService<DTO> where DTO: BaseEntity
    {
        Task<InventoryITActionResult<IEnumerable<DTO>>> GetAllAsync();
        Task<InventoryITActionResult<DTO>> GetByIdAsync(int Id);
        Task<InventoryITActionResult> AddAsync(DTO model);
        Task<InventoryITActionResult> UpdateAsync(DTO model);
        Task<InventoryITActionResult> DeleteAsync(int Id);
    }
}
