﻿using InventoryIT.Core.Models.WorkPlaceModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Services
{
    public interface IWorkPlaceService : IBaseService<WorkPlaceDTO>
    {
    }
}
