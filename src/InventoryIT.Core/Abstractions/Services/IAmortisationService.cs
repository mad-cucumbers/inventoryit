﻿using System.Security.Claims;
using System.Threading.Tasks;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.AmortisationModels;
using InventoryIT.Core.ResponseModels;

namespace InventoryIT.Core.Abstractions.Services
{
    public interface IAmortisationService : IBaseService<AmortisationDTO>
    {
        //Task<InventoryITActionResult<AmortisationDTO>> GetByAmortisationIdAsync(string amorisationId, ClaimsPrincipal principal);
    }
}