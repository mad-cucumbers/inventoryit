﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IOrderRecordRepository : IGenericRepository<OrderRecord>
    {
        Task<OrderRecord> GetByOrderIdAsync(string orderId);
        Task<OrderRecord> GetByInventoryItemIdAsync(string itemId);
        Task<OrderRecord> GetByProductIdAsync(string productId);
        Task<OrderRecord> GetBySerialNumberAsync(string serialNumber);
    }
}
