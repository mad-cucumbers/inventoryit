﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IWorkplaceRepository : IGenericRepository<Workplace>
    {
        Task<IEnumerable<Workplace>> GetByRoomIdAsync(int roomId);
        Task<IEnumerable<Workplace>> GetByBuildingIdAsync(int buildingId);
        Task<IEnumerable<Workplace>> GetByDepartmentIdAsync(int departmentId);
        Task<IEnumerable<Workplace>> GetByRoomAndDepartmentAsync(int roomId, int departmentId);
    }
}