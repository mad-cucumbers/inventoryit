﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IInventoryItemTransactionRepository : IGenericRepository<InventoryItemTransaction>
    {
        Task<IEnumerable<InventoryItemTransaction>> GetByInventoryItemIdAsync(string itemId);

        Task<IEnumerable<InventoryItemTransaction>> GetByOrderIdAsync(string orderId);

        Task<IEnumerable<InventoryItemTransaction>> GetByOperationDate(DateTime operationDate);

        Task<IEnumerable<InventoryItemTransaction>> GetByDistributionIddAsync(int distributionId);

        Task<IEnumerable<InventoryItemTransaction>> GetByAmortisatoinIdAsync(int amortisatoinId);
    }
}
