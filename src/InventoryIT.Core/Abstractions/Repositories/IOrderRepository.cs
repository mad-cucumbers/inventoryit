﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
        Task<Order> GetByOrderIdAsync(string orderId);
        Task<IEnumerable<Order>> GetByOrderDate(DateTime orderDate);
        Task<IEnumerable<Order>> GetByEmployeeIdAsync(int id);
        Task<IEnumerable<Order>> GetByDepartmentIdAsync(string departmentId);
        Task<IEnumerable<Order>> GetByTransactionIdAsync(string transactionId);
    }
}
