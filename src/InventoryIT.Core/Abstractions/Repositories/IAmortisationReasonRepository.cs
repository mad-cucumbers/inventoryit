﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryIT.Core.Domain.Entities;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IAmortisationReasonRepository : IGenericRepository<AmortisationReason>
    {
        Task<AmortisationReason> GetByName(string name);

        Task<IEnumerable<AmortisationReason>> GetByShortDescription(string description);
    }
}
