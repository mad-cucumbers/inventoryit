﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryIT.Core.Domain.Entities;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IAmortisationRecordRepository : IGenericRepository<AmortisationRecord>
    {
        Task<IEnumerable<AmortisationRecord>> GetByAmortisationIdAsync(string amortisationId);

        Task<IEnumerable<AmortisationRecord>> GetByInventoryItemIdAsync(string itemId);

        Task<IEnumerable<AmortisationRecord>> GetByAmortisationReasonIdAsync(string reasonId);
    }
}
