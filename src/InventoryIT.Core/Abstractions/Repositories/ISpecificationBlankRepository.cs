﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface ISpecificationBlankRepository : IGenericRepository<SpecificationBlank>
    {
        Task<SpecificationBlank> GetByBlankIdAsync(string blankId);
    }
}
