﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IDistributionRecordRepository : IGenericRepository<DistributionRecord>
    {
        Task<DistributionRecord> GetByDistributionIdAsync(string distributionId);
        Task<IEnumerable<DistributionRecord>> GetByInventoryItemIdAsync(string itemId);
        Task<IEnumerable<DistributionRecord>> GetByWorkingPlaceIdAsync(int workingPlaceId);
    }
}
