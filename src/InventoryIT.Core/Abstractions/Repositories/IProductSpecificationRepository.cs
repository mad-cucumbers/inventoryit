﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IProductSpecificationRepository : IGenericRepository<ProductSpecification>
    {
        Task<ProductSpecification> GetBySpecificationIdAsync(string specificationId);
        Task<ProductSpecification> GetByProductSpecBlankIdAsync(string specificationBlankId);
    }
}
