﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<Product> GetByProductIdAsync(string producId);
        Task<Product> GetByShortNameAsync(string shortName);
        Task<Product> GetByFullNameAsync(string fullName);
        Task<IEnumerable<Product>> GetByCategoryNameAsync(string category);
        Task<IEnumerable<Product>> GetByVendorAsync(string vendor);
        Task<Product> GetByProductSpecBlankIdAsync(string producSpecBlankId);
    }
}
