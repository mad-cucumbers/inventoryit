﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IDistributionRepository : IGenericRepository<Distribution>
    {
        Task<Distribution> GetByDistributionIdAsync(string distributionId);
        Task<IEnumerable<Distribution>> GetByDIstributionDate(DateTime distributionDate);
        Task<IEnumerable<Distribution>> GetByEmployeeIdAsync(int employeeId);
        Task<IEnumerable<Distribution>> GetByStockEmployeeIdAsync(int employeeId);
    }
}
