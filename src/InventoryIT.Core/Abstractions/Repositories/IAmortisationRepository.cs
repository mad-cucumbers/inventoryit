﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IAmortisationRepository : IGenericRepository<Amortisation>
    {
        Task<Amortisation> GetByAmortisationIdAsync(string amortisationId);
        Task<IEnumerable<Amortisation>> GetByAmortisationDate(DateTime amortisationDate);
        Task<IEnumerable<Amortisation>> GetByEmployeeIdAsync(int id);
    }
}
