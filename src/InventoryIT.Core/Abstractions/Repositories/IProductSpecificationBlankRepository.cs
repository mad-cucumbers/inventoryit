﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IProductSpecificationBlankRepository : IGenericRepository<ProductSpecificationBlank>
    {
        Task<IEnumerable<ProductSpecificationBlank>> GetBySpecificationBlankIdAsync(string itemId);
        Task<IEnumerable<ProductSpecificationBlank>> GetByProductIdAsync(string productId);
        Task<IEnumerable<ProductSpecificationBlank>> GetByProductSpecBlankIdAsync(string itemId);
    }
}
