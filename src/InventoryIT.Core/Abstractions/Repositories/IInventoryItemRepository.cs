﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IInventoryItemRepository : IGenericRepository<InventoryItem>
    {
        Task<IEnumerable<InventoryItem>> GetByInventoryItemIdAsync(string itemId); // Id складской сущности

        Task<IEnumerable<InventoryItem>> GetByProductIdAsync(string productId); // Id номенклатуры

        Task<IEnumerable<InventoryItem>> GetByEntryDate(DateTime entryDate);

        Task<IEnumerable<InventoryItem>> GetByWorkingPlaceIdAsync(int workingPlaceId);

        Task<IEnumerable<InventoryItem>> GetByEmployeeIdAsync(int id);
    }
}
