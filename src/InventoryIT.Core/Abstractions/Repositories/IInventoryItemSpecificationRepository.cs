﻿using InventoryIT.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Abstractions.Repositories
{
    public interface IInventoryItemSpecificationRepository : IGenericRepository<InventoryItemSpecification>
    {
        Task<InventoryItemSpecification> GetBySpecificationIdAsync(string specificationId);
        Task<IEnumerable<InventoryItemSpecification>> GetByInventoryItemIdAsync(string itemId);
    }
}
