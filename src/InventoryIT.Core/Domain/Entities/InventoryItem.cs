﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using InventoryIT.Core.Enums;

namespace InventoryIT.Core.Domain.Entities
{
    public class InventoryItem : BaseEntity
    {
        public string InventoryItemId { get; set; }
        public string OrderRecordId { get; set; }
        public virtual OrderRecord OrderRecord { get; set; }
        public float Quantity { get; set; }
        public string ProductId { get; set; }
        public virtual Product Product { get; set; }
        public DateTime EntryDate { get; set; }
        public int ResponsibleEmployeeId { get; set; }
        public int WorkplaceId { get; set; }
        public virtual Workplace Workplace { get; set; }
        public InventoryItemStatus Status { get; set; }
        public virtual InventoryItemSpecification InventoryItemSpecification { get; set; }
        public virtual List<InventoryItemTransaction> InventoryItemTransactions { get; set; }
        public virtual List<DistributionRecord> DistributionRecords { get; set; }
        public virtual List<AmortisationRecord> AmortisationRecords { get; set; }
    }
}
