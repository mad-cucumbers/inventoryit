﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class Distribution : BaseEntity
    {
        public string DistributionId { get; set; }
        public DateTime DistributionDate { get; set; }
        public int ResponsibleEmployeeId { get; set; }
        public int ResponsibleStockEmployeeId { get; set; }
        public virtual List<DistributionRecord> DistributionRecords { get; set; }
        public int InventoryItemTransactionId { get; set; }
        public virtual InventoryItemTransaction InventoryItemTransaction { get; set; }
    }
}
