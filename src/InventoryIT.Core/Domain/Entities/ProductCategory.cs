﻿using System.Collections.Generic;

namespace InventoryIT.Core.Domain.Entities
{
    public class ProductCategory : BaseEntity
    {
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public bool IsBatch { get; set; }
        public bool IsSpecRequired { get; set; }
        public virtual List<Product> Products { get; set; }
    }
}
