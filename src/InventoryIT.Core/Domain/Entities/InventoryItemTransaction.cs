﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryIT.Core.Enums;

namespace InventoryIT.Core.Domain.Entities
{
    public class InventoryItemTransaction : BaseEntity
    {
        public DateTime OperationDate { get; set; }
        public string InventoryItemId { get; set; }
        public virtual InventoryItem InventoryItem { get; set; }
        public string OrderId { get; set; }
        public virtual Order Order { get; set; }
        public virtual Distribution Distribution { get; set; }
        public virtual Amortisation Amortisation { get; set; }
        public TransactionType TransactionType { get; set; }
        public float Quantity { get; set; }
    }
}
