﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class InventoryItemSpecification : BaseEntity
    {
        public string InventoryItemSpecificationId { get; set; }
        public string InventoryItemId { get; set; }
        public virtual InventoryItem InventoryItem { get; set; }
        public string InventoryItemSpecificationValue { get; set; }
    }
}
