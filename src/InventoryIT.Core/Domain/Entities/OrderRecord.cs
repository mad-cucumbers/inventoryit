﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.Core.Domain.Entities
{
    public class OrderRecord : BaseEntity
    {
        public string OrderRecordId { get; set; }
        public string OrderId { get; set; }
        public virtual Order Order { get; set; }
        public string ProductId { get; set; }
        public virtual Product Product { get; set; }
        public virtual InventoryItem InventoryItem { get; set; }
        public float Quantity { get; set; }
        public string SerialNumber { get; set; }
    }
}
