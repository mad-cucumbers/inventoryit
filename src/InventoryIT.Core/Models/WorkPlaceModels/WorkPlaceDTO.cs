﻿using System.Collections.Generic;
using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.DistributionRecordModels;
using InventoryIT.Core.Models.InventoryItemModels;

namespace InventoryIT.Core.Models.WorkPlaceModels
{
    public class WorkPlaceDTO : BaseEntity
    {
        public int WorkplaceId { get; set; }
        public int BuildingId { get; set; }
        public int RoomId { get; set; }
        public int DepartmentId { get; set; }
        public List<DistributionRecordDTO> DistributionRecords { get; set; }
        public List<InventoryItemDTO> InventoryItems { get; set; }
    }
}
