﻿using System.Collections.Generic;
using InventoryIT.Core.Domain;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.InventoryItemModels;
using InventoryIT.Core.Models.OrderRecordModels;
using InventoryIT.Core.Models.ProductCategoryModels;
using InventoryIT.Core.Models.ProductSpecificationBlankModels;

namespace InventoryIT.Core.Models.ProductModels
{
    public class ProductDTO : BaseEntity
    {
        public string ProductId { get; set; }
        public ProductSpecificationBlankDTO ProductSpecificationBlank { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public string CategoryName { get; set; }
        public ProductCategoryDTO ProductCategory { get; set; }
        public string Vendor { get; set; }
        public List<InventoryItemDTO> InventoryItems { get; set; }
        public List<OrderRecordDTO> OrderRecords { get; set; }

        public string ProductSpecificationBlankId { get; set; }
        public int ProductCategoryId { get; set; }
    }
}
