﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.AmortisationRecordModels;
using InventoryIT.Core.Models.InventoryItemTransactionModels;
using System;
using System.Collections.Generic;

namespace InventoryIT.Core.Models.AmortisationModels
{
    public class AmortisationDTO : BaseEntity
    {
        public string AmortisationId { get; set; }
        public InventoryItemTransactionDTO InventoryItemTransaction { get; set; }
        public DateTime AmortisationDate { get; set; }
        public List<AmortisationRecordDTO> AmortisationRecords { get; set; }

        public int InventoryItemTransactionId { get; set; }
        public int ResponsibleEmployeeId { get; set; }
    }
}