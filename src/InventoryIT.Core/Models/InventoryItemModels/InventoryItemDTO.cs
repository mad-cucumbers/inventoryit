﻿using System;
using System.Collections.Generic;
using InventoryIT.Core.Domain;
using InventoryIT.Core.Enums;
using InventoryIT.Core.Models.AmortisationRecordModels;
using InventoryIT.Core.Models.DistributionRecordModels;
using InventoryIT.Core.Models.InventoryItemSpecificationModels;
using InventoryIT.Core.Models.InventoryItemTransactionModels;
using InventoryIT.Core.Models.OrderRecordModels;
using InventoryIT.Core.Models.ProductModels;
using InventoryIT.Core.Models.WorkPlaceModels;

namespace InventoryIT.Core.Models.InventoryItemModels
{
    public class InventoryItemDTO : BaseEntity
    {
        public string InventoryItemId { get; set; }
        public OrderRecordDTO OrderRecord { get; set; }
        public float Quantity { get; set; }
        public ProductDTO Product { get; set; }
        public DateTime EntryDate { get; set; }
        public WorkPlaceDTO Workplace { get; set; }
        public InventoryItemStatus Status { get; set; }
        public InventoryItemSpecificationDTO InventoryItemSpecification { get; set; }
        public List<InventoryItemTransactionDTO> InventoryItemTransactions { get; set; }
        public List<DistributionRecordDTO> DistributionRecords { get; set; }
        public List<AmortisationRecordDTO> AmortisationRecords { get; set; }

        public int OrderRecordId { get; set; }
        public int ProductId { get; set; }
        public int EmployeeId { get; set; }
        public int WorkplaceId { get; set; }
        public int InventoryItemSpecificationId { get; set; }
    }
}
