﻿using System.Collections.Generic;
using InventoryIT.Core.Domain;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.ProductModels;
using InventoryIT.Core.Models.ProductSpecificationModels;
using InventoryIT.Core.Models.SpecificationBlankModels;

namespace InventoryIT.Core.Models.ProductSpecificationBlankModels
{
    public class ProductSpecificationBlankDTO : BaseEntity
    {
        public string ProductSpecificationBlankId { get; set; }
        public string SpecificationBlankId { get; set; }
        public SpecificationBlankDTO SpecificationBlank { get; set; }
        public string ProductId { get; set; }
        public ProductDTO Product { get; set; }
        public List<ProductSpecificationDTO> ProductSpecifications { get; set; }
    }
}
