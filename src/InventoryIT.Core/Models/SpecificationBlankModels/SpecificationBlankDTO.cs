﻿using System.Collections.Generic;
using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.ProductSpecificationBlankModels;
using InventoryIT.Core.Models.SpecificationModels;

namespace InventoryIT.Core.Models.SpecificationBlankModels
{
    public class SpecificationBlankDTO : BaseEntity
    {
        public string SpecificationBlankId { get; set; }
        public List<SpecificationDTO> Specifications { get; set; }
        public List<ProductSpecificationBlankDTO> ProductSpecificationBlanks { get; set; }
    }
}
