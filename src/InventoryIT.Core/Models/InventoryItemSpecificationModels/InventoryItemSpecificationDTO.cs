﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.InventoryItemModels;

namespace InventoryIT.Core.Models.InventoryItemSpecificationModels
{
    public class InventoryItemSpecificationDTO : BaseEntity
    {
        public string InventoryItemSpecificationId { get; set; }
        public InventoryItemDTO InventoryItem { get; set; }
        public string InventoryItemSpecificationValue { get; set; }

        public int InventoryItemId { get; set; }
    }
}
