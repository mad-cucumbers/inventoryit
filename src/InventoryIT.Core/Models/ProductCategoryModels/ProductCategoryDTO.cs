﻿using System.Collections.Generic;
using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.ProductModels;

namespace InventoryIT.Core.Models.ProductCategoryModels
{
    public class ProductCategoryDTO : BaseEntity
    {
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public bool IsBatch { get; set; }
        public bool IsSpecRequired { get; set; }
        public List<ProductDTO> Products { get; set; }
    }
}
