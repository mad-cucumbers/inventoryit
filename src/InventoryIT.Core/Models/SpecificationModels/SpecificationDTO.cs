﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.SpecificationBlankModels;

namespace InventoryIT.Core.Models.SpecificationModels
{
    public class SpecificationDTO : BaseEntity
    {
        public string SpecificationId { get; set; }//уникальное
        public bool IsRequered { get; set; }
        public int SpecificationBlankId { get; set; }

        public SpecificationBlankDTO SpecificationBlank { get; set; }
    }
}
