﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.DistributionModels;
using InventoryIT.Core.Models.InventoryItemModels;
using InventoryIT.Core.Models.WorkPlaceModels;

namespace InventoryIT.Core.Models.DistributionRecordModels
{
    public class DistributionRecordDTO : BaseEntity
    {
        public int WorkplaceId { get; set; }
        public int DistributionId { get; set; }
        public int InventoryItemId { get; set; }
        public float Quantity { get; set; }

        public DistributionDTO Distribution { get; set; }
        public InventoryItemDTO InventoryItem { get; set; }
        public WorkPlaceDTO Workplace { get; set; }
    }
}
