﻿using System;
using System.Collections.Generic;
using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.DistributionRecordModels;
using InventoryIT.Core.Models.InventoryItemTransactionModels;

namespace InventoryIT.Core.Models.DistributionModels
{
    public class DistributionDTO : BaseEntity
    {
        public string DistributionId { get; set; }
        public DateTime DistributionDate { get; set; }
        public List<DistributionRecordDTO> DistributionRecords { get; set; }       
        public InventoryItemTransactionDTO InventoryItemTransaction { get; set; }

        public int ResponsibleStockEmployeeId { get; set; }
        public int ResponsibleEmployeeId { get; set; }
        public string InventoryItemTransactionId { get; set; }
    }
}
