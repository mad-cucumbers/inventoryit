﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.InventoryItemTransactionModels;
using InventoryIT.Core.Models.OrderRecordModels;
using System;
using System.Collections.Generic;

namespace InventoryIT.Core.Models.OrderModels
{
    public class OrderDTO : BaseEntity
    {
        public string OrderId { get; set; }
        public InventoryItemTransactionDTO InventoryItemTransaction { get; set; }
        public DateTime OrderDate { get; set; }
        public string Comment { get; set; }
        public List<OrderRecordDTO> OrderRecords { get; set; }

        public int InventoryItemTransactionId { get; set; }
        public int DepartmentId { get; set; }
        public int EmployeeId { get; set; }
    }
}
