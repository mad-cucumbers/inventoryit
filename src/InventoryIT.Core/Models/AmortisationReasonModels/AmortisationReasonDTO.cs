﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.AmortisationRecordModels;

namespace InventoryIT.Core.Models.AmortisationReasonModels
{
    public class AmortisationReasonDTO : BaseEntity
    {
        public int AmortisationReasonId { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public List<AmortisationRecordDTO> AmortisationRecords { get; set; }
    }
}

