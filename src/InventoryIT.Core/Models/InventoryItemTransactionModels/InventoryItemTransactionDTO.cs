﻿using System;
using InventoryIT.Core.Domain;
using InventoryIT.Core.Enums;
using InventoryIT.Core.Models.AmortisationModels;
using InventoryIT.Core.Models.DistributionModels;
using InventoryIT.Core.Models.InventoryItemModels;
using InventoryIT.Core.Models.OrderModels;

namespace InventoryIT.Core.Models.InventoryItemTransactionModels
{
    public class InventoryItemTransactionDTO : BaseEntity
    {
        public DateTime OperationDate { get; set; }
        public InventoryItemDTO InventoryItem { get; set; }
        public OrderDTO Order { get; set; }
        public DistributionDTO Distribution { get; set; }
        public AmortisationDTO Amortisation { get; set; }
        public TransactionType TransactionType { get; set; }
        public float Quantity { get; set; }

        public int InventoryItemId { get; set; }
        public int OrderId { get; set; }
        public int DistributionId { get; set; }
        public int AmortisationId { get; set; }
    }
}
