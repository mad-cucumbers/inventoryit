﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.InventoryItemModels;
using InventoryIT.Core.Models.OrderModels;
using InventoryIT.Core.Models.ProductModels;

namespace InventoryIT.Core.Models.OrderRecordModels
{
    public class OrderRecordDTO : BaseEntity
    {
        public string OrderRecordId { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int InventoryItemId { get; set; }

        public OrderDTO Order { get; set; }
        public ProductDTO Product { get; set; }
        public InventoryItemDTO InventoryItem { get; set; }
        public float Quantity { get; set; }
        public string SerialNumber { get; set; }
    }
}
