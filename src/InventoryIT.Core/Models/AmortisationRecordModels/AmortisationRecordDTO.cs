﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.AmortisationModels;
using InventoryIT.Core.Models.AmortisationReasonModels;
using InventoryIT.Core.Models.InventoryItemModels;

namespace InventoryIT.Core.Models.AmortisationRecordModels
{
    public class AmortisationRecordDTO : BaseEntity
    {
        public AmortisationDTO Amortisation { get; set; }
        public InventoryItemDTO InventoryItem { get; set; }
        public float Quantity { get; set; }
        public AmortisationReasonDTO AmortisationReason { get; set; }

        public int AmortisationId { get; set; }
        public int InventoryItemId { get; set; }
        public int AmortisationReasonId { get; set; }
    }
}
