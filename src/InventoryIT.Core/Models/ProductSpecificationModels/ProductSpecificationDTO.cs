﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.ProductSpecificationBlankModels;

namespace InventoryIT.Core.Models.ProductSpecificationModels
{
    public class ProductSpecificationDTO : BaseEntity
    {
        public string ProductSpecificationId { get; set; }
        public string ProductSpecificationValue { get; set; }
        public string ProductSpecificationBlankId { get; set; }

        public ProductSpecificationBlankDTO ProductSpecificationBlank { get; set; }
    }
}
