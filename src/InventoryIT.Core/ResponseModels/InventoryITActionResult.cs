﻿using System.Collections.Generic;
using Contracts.Enums;
using InventoryIT.Core.Enums;

namespace InventoryIT.Core.ResponseModels
{
    public class InventoryITActionResult
    {
        public bool Success { get; set; }
        public IEnumerable<TypeOfErrors> Errors { get; set; }
        public NotificationType Type { get; set; }
        public string TypeStr { get { return Type.ToString(); } }
        public string ErrorDescription { get; set; }
        public string AspNetException { get; set; }

        protected InventoryITActionResult(bool success, NotificationType type)
        {
            Success = success;
            Type = type;
        }
        protected InventoryITActionResult(bool success,
                                          IEnumerable<TypeOfErrors> errors,
                                          string errorDescription,
                                          NotificationType type, string e)
        {
            Success = success;
            Errors = errors;
            ErrorDescription = errorDescription;
            Type = type;
            AspNetException = e;
        }

        public static InventoryITActionResult IsSuccess()
        { 
            return new InventoryITActionResult(true, NotificationType.Success); 
        }

        public static InventoryITActionResult Fail(IEnumerable<TypeOfErrors> errors, string e = null,
                                                   string errorDescription = "Произошла внутренняя ошибка",
                                                   NotificationType type = NotificationType.Error)
        { 
            return new InventoryITActionResult(false, errors, errorDescription, type, e);
        }
    }

    public class InventoryITActionResult<T> : InventoryITActionResult
    {
        public T Data { get; private set; }

        protected InventoryITActionResult(bool success, NotificationType type, T data)
            : base(success, type) => Data = data;

        protected InventoryITActionResult(bool success,
                                          IEnumerable<TypeOfErrors> errors,
                                          string errorDescription,
                                          NotificationType type, string e,
                                          T data) 
            : base(success, errors, errorDescription, type, e) => Data = data;


        public static InventoryITActionResult<T> IsSuccess(T data)
        {
            return new InventoryITActionResult<T>(true, NotificationType.Success, data);
        }

        public static InventoryITActionResult<T> Fail(T data, 
                                                      IEnumerable<TypeOfErrors> errors, string e = null,
                                                      string errorDescription = "Произошла внутренняя ошибка",
                                                      NotificationType type = NotificationType.Error)
        {
            return new InventoryITActionResult<T>(false, errors, errorDescription, type, e, data);
        }
    }
}