﻿using Contracts.Enums;
using InventoryIT.Core.Abstractions;
using InventoryIT.Core.Domain;
using InventoryIT.Core.ResponseModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected DataContext _context;

        protected Expression<Func<T, object>>[] Includes;

        public GenericRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<InventoryITActionResult<IEnumerable<T>>> GetAllAsync()
        {
            try
            {
                var response = await _context.Set<T>().ToListAsync();
                return InventoryITActionResult<IEnumerable<T>>.IsSuccess(response);
            }
            catch (Exception e)
            {
                return InventoryITActionResult<IEnumerable<T>>.Fail(null, new[] { TypeOfErrors.InternalServerError },
                    $"Произошла ошибка в базе данных || Запрос на получение списка моделей || Модель: < {typeof(IEnumerable<T>)} > || Описание: < {e.InnerException} >");
            }
            
        }

        public async Task<InventoryITActionResult<T>> GetEntityByIdAsync(int id)
        {
            try
            {
                IQueryable<T> set = _context.Set<T>();
                if (Includes != null) set = Includes.Aggregate(set, (current, IncludeProp) => current.Include(IncludeProp));
                var response = await set.FirstOrDefaultAsync(x => x.Id == id);
                return InventoryITActionResult<T>.IsSuccess(response);
            }
            catch (Exception e)
            {
                return InventoryITActionResult<T>.Fail(null, new[] { TypeOfErrors.InternalServerError },
                    $"Ошибка при поиске модели || Модель: < {typeof(T)} > || Входной параметр ID: < {id} > || Описание: < {e.InnerException} >");

            }
        }

        public async Task<InventoryITActionResult> AddAsync(T entity)
        {
            try
            {
                _context.Set<T>().Add(entity);
                await _context.SaveChangesAsync();
                return InventoryITActionResult.IsSuccess();
            }
            catch (Exception e)
            {
                return InventoryITActionResult<T>.Fail(null, new[] { TypeOfErrors.InternalServerError },
                    $"Ошибка добавления модели || Модель < {typeof(T)} > || ID < {entity?.Id} > || {e.InnerException} >");

            }
        }

        public async Task<InventoryITActionResult> UpdateAsync(T entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return InventoryITActionResult.IsSuccess();
            }
            catch (Exception e)
            {
                return InventoryITActionResult<T>.Fail(null, new[] { TypeOfErrors.InternalServerError },
                    $"Ошибка изменения модели || Модель < {typeof(T)} > || ID < {entity?.Id} > || {e.InnerException} >");

            }
        }

        public async Task<InventoryITActionResult> DeleteAsync(T entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Deleted;
                await _context.SaveChangesAsync();
                return InventoryITActionResult.IsSuccess();
            }
            catch (Exception e)
            {
                return InventoryITActionResult<T>.Fail(null, new[] { TypeOfErrors.InternalServerError },
                    $"Ошибка удаление модели || Модель < {typeof(T)} > || ID < {entity?.Id} > || {e.InnerException} >");

            }
        }
    }
}
