﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class WorkplaceRepository : GenericRepository<Workplace>, IWorkplaceRepository
    {
        public WorkplaceRepository(DataContext context) : base(context)
        {
            //Includes = new Expression<Func<Workplace, object>>[]
            //{
            //    a => a.Room,
            //    a => a.Building,
            //    a => a.Department
            //};
        }

        public async Task<IEnumerable<Workplace>> GetByRoomIdAsync(int roomId)
        {
            IQueryable<Workplace> set = _context.Workplaces;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.RoomId == roomId).ToListAsync();
        }

        public async Task<IEnumerable<Workplace>> GetByBuildingIdAsync(int buildingId)
        {
            IQueryable<Workplace> set = _context.Workplaces;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.BuildingId == buildingId).ToListAsync();
        }

        public async Task<IEnumerable<Workplace>> GetByDepartmentIdAsync(int departmentId)
        {
            IQueryable<Workplace> set = _context.Workplaces;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.DepartmentId == departmentId).ToListAsync();
        }

        public async Task<IEnumerable<Workplace>> GetByRoomAndDepartmentAsync(int roomId, int departmentId)
        {
            IQueryable<Workplace> set = _context.Workplaces;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.RoomId == roomId && x.DepartmentId == departmentId).ToListAsync();
        }
    }
}
