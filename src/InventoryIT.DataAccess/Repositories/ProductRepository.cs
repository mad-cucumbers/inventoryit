﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<Product, object>>[]
            {
                a => a.ProductCategory,
                a => a.ProductSpecificationBlank
            };
        }

        public async Task<IEnumerable<Product>> GetByCategoryNameAsync(string categoryName)
        {
            IQueryable<Product> set = _context.Products;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
                }

            return await set.Where(x => x.CategoryName.Equals(categoryName)).ToListAsync();
        }

        public async Task<Product> GetByFullNameAsync(string fullName)
        {
            IQueryable<Product> set = _context.Products;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
                }

            return await set.FirstOrDefaultAsync(x => x.FullName.Equals(fullName));
        }

        public async Task<Product> GetByProductIdAsync(string producId)
        {
            IQueryable<Product> set = _context.Products;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.ProductId.Equals(producId));
        }

        public async Task<Product> GetByShortNameAsync(string shortName)
        {
            IQueryable<Product> set = _context.Products;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.ShortName.Equals(shortName));
        }

        public async Task<IEnumerable<Product>> GetByVendorAsync(string vendor)
        {
            IQueryable<Product> set = _context.Products;
            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.Vendor.Equals(vendor)).ToListAsync();
        }

        public async Task<Product> GetByProductSpecBlankIdAsync(string producSpecBlankId)
        {
            IQueryable<Product> set = _context.Products;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.ProductSpecificationBlank.ProductSpecificationBlankId.Equals(producSpecBlankId));
        }
    }
}
