﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace InventoryIT.DataAccess.Repositories
{
    public class DistributionRecordRepository : GenericRepository<DistributionRecord>, IDistributionRecordRepository
    {
        public DistributionRecordRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<DistributionRecord, object>>[]
            {
                a => a.Distribution,
                a => a.InventoryItem,
                a => a.Workplace
            };
        }

        public async Task<DistributionRecord> GetByDistributionIdAsync(string distributionId)
        {
            IQueryable<DistributionRecord> set = _context.DistributionRecords;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.DistributionId.Equals(distributionId));
        }

        public async Task<IEnumerable<DistributionRecord>> GetByInventoryItemIdAsync(string itemId)
        {
            IQueryable<DistributionRecord> set = _context.DistributionRecords;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.InventoryItemId.Equals(itemId)).ToListAsync();
        }

        public async Task<IEnumerable<DistributionRecord>> GetByWorkingPlaceIdAsync(int workingPlaceId)
        {
            IQueryable<DistributionRecord> set = _context.DistributionRecords;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.WorkplaceId == workingPlaceId).ToListAsync();
        }
    }
}
