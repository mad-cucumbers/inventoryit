﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace InventoryIT.DataAccess.Repositories
{
    public class AmortisationReasonRepository : GenericRepository<AmortisationReason>, IAmortisationReasonRepository
    {
        public AmortisationReasonRepository(DataContext context) : base(context)
        {
        }

        public async Task<AmortisationReason> GetByName(string name)
        {
            IQueryable<AmortisationReason> set = _context.AmortisationReasons;

            //if (Includes != null)
            //{
            //    set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            //}

            return await set.FirstOrDefaultAsync(x => x.Name.Equals(name));
        }

        public async Task<IEnumerable<AmortisationReason>> GetByShortDescription(string description)
        {
            IQueryable<AmortisationReason> set = _context.AmortisationReasons;

            //if (Includes != null)
            //{
            //    set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            //}

            return await set.Where(x => x.ShortDescription.Contains(description)).ToListAsync();
        }
    }
}
