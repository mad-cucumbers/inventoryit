﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class AmortisationRecordRepository : GenericRepository<AmortisationRecord>, IAmortisationRecordRepository
    {
        public AmortisationRecordRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<AmortisationRecord, object>>[]
            {
                x => x.Amortisation,
                x => x.InventoryItem,
                x => x.AmortisationReason
            };
        }

        public async Task<IEnumerable<AmortisationRecord>> GetByAmortisationIdAsync(string amortisationId)
        {
            IQueryable<AmortisationRecord> set = _context.AmortisationRecords;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.AmortisationId.Equals(amortisationId)).ToListAsync();
        }

        public async Task<IEnumerable<AmortisationRecord>> GetByInventoryItemIdAsync(string itemId)
        {
            IQueryable<AmortisationRecord> set = _context.AmortisationRecords;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.InventoryItemId.Equals(itemId)).ToListAsync();
        }

        public async Task<IEnumerable<AmortisationRecord>> GetByAmortisationReasonIdAsync(string reasonId)
        {
            IQueryable<AmortisationRecord> set = _context.AmortisationRecords;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.AmortisationReasonId.Equals(reasonId)).ToListAsync();
        }
    }
}
