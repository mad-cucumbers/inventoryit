﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class ProductSpecificationBlankRepository : GenericRepository<ProductSpecificationBlank>, IProductSpecificationBlankRepository
    {
        public ProductSpecificationBlankRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<ProductSpecificationBlank, object>>[]
            {
                a => a.Product,
                a => a.SpecificationBlank
            };
        }

        public async Task<IEnumerable<ProductSpecificationBlank>> GetBySpecificationBlankIdAsync(string itemId)
        {
            IQueryable<ProductSpecificationBlank> set = _context.ProductSpecificationBlanks;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.SpecificationBlankId.Equals(itemId)).ToListAsync();
        }

        public async Task<IEnumerable<ProductSpecificationBlank>> GetByProductIdAsync(string productId)
        {
            IQueryable<ProductSpecificationBlank> set = _context.ProductSpecificationBlanks;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.ProductId.Equals(productId)).ToListAsync();
        }

        public async Task<IEnumerable<ProductSpecificationBlank>> GetByProductSpecBlankIdAsync(string itemId)
        {
            IQueryable<ProductSpecificationBlank> set = _context.ProductSpecificationBlanks;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.ProductSpecificationBlankId.Equals(itemId)).ToListAsync();
        }
    }
}
