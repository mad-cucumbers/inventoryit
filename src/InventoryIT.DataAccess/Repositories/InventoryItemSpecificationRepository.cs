﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class InventoryItemSpecificationRepository : GenericRepository<InventoryItemSpecification>, IInventoryItemSpecificationRepository
    {
        public InventoryItemSpecificationRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<InventoryItemSpecification, object>>[]
            {
                //a => a.Specification
                a => a.InventoryItem
            };
        }

        public async Task<InventoryItemSpecification> GetBySpecificationIdAsync(string specificationId)
        {
            IQueryable<InventoryItemSpecification> set = _context.InventoryItemSpecifications;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.InventoryItemSpecificationId.Equals(specificationId));
        }

        public async Task<IEnumerable<InventoryItemSpecification>> GetByInventoryItemIdAsync(string itemId)
        {
            IQueryable<InventoryItemSpecification> set = _context.InventoryItemSpecifications;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.InventoryItemId.Equals(itemId)).ToListAsync();
        }
    }
}
