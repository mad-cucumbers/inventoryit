﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class SpecificationBlankRepository : GenericRepository<SpecificationBlank>, ISpecificationBlankRepository
    {
        public SpecificationBlankRepository(DataContext context) : base(context)
        {
        }

        public async Task<SpecificationBlank> GetByBlankIdAsync(string blankId)
        {
            IQueryable<SpecificationBlank> set = _context.SpecificationBlanks;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.SpecificationBlankId.Equals(blankId));
        }
    }
}
