﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class AmortisationRepository : GenericRepository<Amortisation>, IAmortisationRepository
    {
        public AmortisationRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<Amortisation, object>>[]
            {
                //a => a.ResponsibleEmployee
                a => a.InventoryItemTransaction
            };
        }

        public async Task<Amortisation> GetByAmortisationIdAsync(string amortisationId)
        {
            IQueryable<Amortisation> set = _context.Amortisations;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.AmortisationId.Equals(amortisationId));
        }

        public async Task<IEnumerable<Amortisation>> GetByAmortisationDate(DateTime amortisationDate)
        {
            IQueryable<Amortisation> set = _context.Amortisations;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.AmortisationDate == amortisationDate).ToListAsync(); ;
        }

        public async Task<IEnumerable<Amortisation>> GetByEmployeeIdAsync(int id)
        {
            IQueryable<Amortisation> set = _context.Amortisations;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.ResponsibleEmployeeId == id).ToListAsync();
        }
    }
}
