﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class DistributionRepository : GenericRepository<Distribution>, IDistributionRepository
    {
        public DistributionRepository(DataContext context) : base(context)
        {
            Includes = new System.Linq.Expressions.Expression<Func<Distribution, object>>[]
            {
                a => a.InventoryItemTransaction
            };
        }

        public async Task<Distribution> GetByDistributionIdAsync(string distributionId)
        {
            IQueryable<Distribution> set = _context.Distributions;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.DistributionId.Equals(distributionId));
        }

        public async Task<IEnumerable<Distribution>> GetByDIstributionDate(DateTime distributionDate)
        {
            IQueryable<Distribution> set = _context.Distributions;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.DistributionDate == distributionDate).ToListAsync(); ;
        }

        public async Task<IEnumerable<Distribution>> GetByEmployeeIdAsync(int employeeId)
        {
            IQueryable<Distribution> set = _context.Distributions;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.ResponsibleEmployeeId == employeeId).ToListAsync();
        }

        public async Task<IEnumerable<Distribution>> GetByStockEmployeeIdAsync(int employeeId)
        {
            IQueryable<Distribution> set = _context.Distributions;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.ResponsibleStockEmployeeId == employeeId).ToListAsync();
        }
    }
}
