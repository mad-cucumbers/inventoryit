﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<Order, object>>[]
            {
                a => a.InventoryItemTransaction
            };
        }

        public async Task<Order> GetByOrderIdAsync(string orderId)
        {
            IQueryable<Order> set = _context.Orders;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.OrderId.Equals(orderId));
        }

        public async Task<IEnumerable<Order>> GetByOrderDate(DateTime orderDate)
        {
            IQueryable<Order> set = _context.Orders;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.OrderDate == orderDate).ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetByEmployeeIdAsync(int id)
        {
            IQueryable<Order> set = _context.Orders;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.EmployeeId == id).ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetByDepartmentIdAsync(string departmentId)
        {
            IQueryable<Order> set = _context.Orders;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.DepartmentId.Equals(departmentId)).ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetByTransactionIdAsync(string transactionId)
        {
            IQueryable<Order> set = _context.Orders;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.InventoryItemTransaction.Id.Equals(transactionId)).ToListAsync();
        }
    }
}
