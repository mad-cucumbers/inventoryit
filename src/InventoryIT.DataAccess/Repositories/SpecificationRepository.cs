﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class SpecificationRepository : GenericRepository<Specification>, ISpecificationRepository
    {
        public SpecificationRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<Specification, object>>[]
            {
                a => a.SpecificationBlank
            };
        }

        public async Task<Specification> GetBySpecificationIdAsync(string specId)
        {
            IQueryable<Specification> set = _context.Specifications;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.SpecificationId.Equals(specId));
        }

        public async Task<IEnumerable<Specification>> GetBySpecBlankIdAsync(string blankId)
        {
            IQueryable<Specification> set = _context.Specifications;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.SpecificationBlankId.Equals(blankId)).ToListAsync();
        }
    }
}
