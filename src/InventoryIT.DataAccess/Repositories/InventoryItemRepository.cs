﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class InventoryItemRepository : GenericRepository<InventoryItem>, IInventoryItemRepository
    {
        public InventoryItemRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<InventoryItem, object>> []
            {
                a => a.Product,
                a => a.OrderRecord,
                a => a.InventoryItemSpecification
            };
        }

        public async Task<IEnumerable<InventoryItem>> GetByEntryDate(DateTime entryDate)
        {
            IQueryable<InventoryItem> set = _context.InventoryItems;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.EntryDate == entryDate).ToListAsync();
        }

        public async Task<IEnumerable<InventoryItem>> GetByInventoryItemIdAsync(string itemId)
        {
            IQueryable<InventoryItem> set = _context.InventoryItems;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
                }

            return await set.Where(x => x.InventoryItemId.Equals(itemId)).ToListAsync();
        }

        public async Task<IEnumerable<InventoryItem>> GetByProductIdAsync(string productId)
        {
            IQueryable<InventoryItem> set = _context.InventoryItems;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.ProductId.Equals(productId)).ToListAsync();
        }

        public async Task<IEnumerable<InventoryItem>> GetByWorkingPlaceIdAsync(int workingPlaceId)
        {
            IQueryable<InventoryItem> set = _context.InventoryItems;

            if (Includes != null)
                {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.WorkplaceId == workingPlaceId).ToListAsync();
        }

        public async Task<IEnumerable<InventoryItem>> GetByEmployeeIdAsync(int id)
        {
            IQueryable<InventoryItem> set = _context.InventoryItems;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.ResponsibleEmployeeId == id).ToListAsync();
        }
    }
}
