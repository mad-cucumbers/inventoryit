﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class OrderRecordRepository : GenericRepository<OrderRecord>, IOrderRecordRepository
    {
        public OrderRecordRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<OrderRecord, object>>[]
            {
                a => a.Order,
                a => a.Product,
                a => a.InventoryItem
            };
        }

        public async Task<OrderRecord> GetByOrderIdAsync(string orderId)
        {
            IQueryable<OrderRecord> set = _context.OrderRecords;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.OrderId.Equals(orderId));
        }

        public async Task<OrderRecord> GetByInventoryItemIdAsync(string itemId)
        {
            IQueryable<OrderRecord> set = _context.OrderRecords;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.InventoryItem.InventoryItemId.Equals(itemId));
        }

        public async Task<OrderRecord> GetByProductIdAsync(string productId)
        {
            IQueryable<OrderRecord> set = _context.OrderRecords;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.ProductId.Equals(productId));
        }
        public async Task<OrderRecord> GetBySerialNumberAsync(string serialNumber)
        {
            IQueryable<OrderRecord> set = _context.OrderRecords;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.SerialNumber.Equals(serialNumber));
        }
    }
}
