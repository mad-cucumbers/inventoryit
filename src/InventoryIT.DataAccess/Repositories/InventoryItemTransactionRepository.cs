﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class InventoryItemTransactionRepository : GenericRepository<InventoryItemTransaction>, IInventoryItemTransactionRepository
    {
        public InventoryItemTransactionRepository(DataContext context) : base (context)
        {
            Includes = new Expression<Func<InventoryItemTransaction, object>>[]
            {
                a => a.Order,
                a => a.InventoryItem,
                a => a.Distribution,
                a => a.Amortisation
            };
        }

        public async Task<IEnumerable<InventoryItemTransaction>> GetByAmortisatoinIdAsync(int amortisatoinId)
        {
            IQueryable<InventoryItemTransaction> set = _context.InventoryItemTransactions;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.Amortisation.AmortisationId.Equals(amortisatoinId)).ToListAsync();
        }

        public async Task<IEnumerable<InventoryItemTransaction>> GetByDistributionIddAsync(int distributionId)
        {
            IQueryable<InventoryItemTransaction> set = _context.InventoryItemTransactions;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.Distribution.DistributionId.Equals(distributionId)).ToListAsync();
        }

        public async Task<IEnumerable<InventoryItemTransaction>> GetByInventoryItemIdAsync(string itemId)
        {
            IQueryable<InventoryItemTransaction> set = _context.InventoryItemTransactions;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.InventoryItemId.Equals(itemId)).ToListAsync();
        }

        public async Task<IEnumerable<InventoryItemTransaction>> GetByOperationDate(DateTime operationDate)
        {
            IQueryable<InventoryItemTransaction> set = _context.InventoryItemTransactions;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.OperationDate == operationDate).ToListAsync();
        }

        public async Task<IEnumerable<InventoryItemTransaction>> GetByOrderIdAsync(string orderId)
        {
            IQueryable<InventoryItemTransaction> set = _context.InventoryItemTransactions;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.Where(x => x.OrderId.Equals(orderId)).ToListAsync();
        }
    }
}
