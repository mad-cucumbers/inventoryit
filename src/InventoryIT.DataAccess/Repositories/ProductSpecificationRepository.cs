﻿using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Repositories
{
    public class ProductSpecificationRepository : GenericRepository<ProductSpecification>, IProductSpecificationRepository
    {
        public ProductSpecificationRepository(DataContext context) : base(context)
        {
            Includes = new Expression<Func<ProductSpecification, object>>[]
            {
                //a => a.Specification,
                a => a.ProductSpecificationBlank
            };
        }

        public async Task<ProductSpecification> GetBySpecificationIdAsync(string specificationId)
        {
            IQueryable<ProductSpecification> set = _context.ProductSpecifications;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.ProductSpecificationId.Equals(specificationId));
        }

        public async Task<ProductSpecification> GetByProductSpecBlankIdAsync(string specificationBlankId)
        {
            IQueryable<ProductSpecification> set = _context.ProductSpecifications;

            if (Includes != null)
            {
                set = Includes.Aggregate(set, (current, includeProp) => current.Include(includeProp));
            }

            return await set.FirstOrDefaultAsync(x => x.ProductSpecificationBlankId.Equals(specificationBlankId));
        }
    }
}
