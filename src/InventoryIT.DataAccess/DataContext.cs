﻿using InventoryIT.Core.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace InventoryIT.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Amortisation> Amortisations { get; set; }
        public DbSet<AmortisationReason> AmortisationReasons { get; set; }
        public DbSet<AmortisationRecord> AmortisationRecords { get; set; }
        public DbSet<Distribution> Distributions { get; set; }
        public DbSet<DistributionRecord> DistributionRecords { get; set; }
        public DbSet<InventoryItemSpecification> InventoryItemSpecifications { get; set; }
        public DbSet<InventoryItem> InventoryItems { get; set; }
        public DbSet<InventoryItemTransaction> InventoryItemTransactions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<ProductSpecification> ProductSpecifications { get; set; }
        public DbSet<ProductSpecificationBlank> ProductSpecificationBlanks { get; set; }
        public DbSet<OrderRecord> OrderRecords { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<SpecificationBlank> SpecificationBlanks { get; set; }
        public DbSet<Specification> Specifications { get; set; }
        public DbSet<Workplace> Workplaces { get; set; }

        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasKey(r => r.Id);
                entity.HasIndex(r => r.OrderId).IsUnique();
                entity.Property(r => r.OrderId).IsRequired();
                entity.Property(r => r.OrderDate).IsRequired();
                entity.Property(r => r.DepartmentId).IsRequired();
                entity.Property(r => r.EmployeeId).IsRequired();

                entity.HasOne(r => r.InventoryItemTransaction)
                      .WithOne(e => e.Order)
                      .HasForeignKey<InventoryItemTransaction>(i => i.OrderId)
                      .HasPrincipalKey<Order>(o => o.OrderId);
            });

            modelBuilder.Entity<OrderRecord>(entity =>
            {
                entity.HasKey(r => r.Id);
                entity.Property(r => r.OrderId).IsRequired();
                entity.Property(r => r.Quantity).IsRequired();
                entity.Property(r => r.ProductId).IsRequired();
                entity.Property(r => r.SerialNumber).IsRequired();

                entity.HasOne(rl => rl.Order)
                    .WithMany(rt => rt.OrderRecords)
                    .HasForeignKey(rl => rl.OrderId)
                    .HasPrincipalKey(rt => rt.OrderId);

                entity.HasOne(rl => rl.Product)
                    .WithMany(p => p.OrderRecords)
                    .HasForeignKey(rl => rl.OrderId)
                    .HasPrincipalKey(p => p.ProductId);

                entity.HasOne(rl => rl.InventoryItem)
                    .WithOne(rt => rt.OrderRecord)
                    .HasForeignKey<InventoryItem>(rl => rl.OrderRecordId)
                    .HasPrincipalKey<OrderRecord>(ii => ii.OrderRecordId);
            });

            modelBuilder.Entity<ProductCategory>(entity =>
            {
                entity.HasKey(p => p.Id);
                entity.HasIndex(p => p.CategoryName).IsUnique();
                entity.Property(p => p.CategoryName).IsRequired();
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(p => p.Id);
                entity.HasIndex(p => p.ProductId).IsUnique();
                entity.Property(p => p.CategoryName).IsRequired();

                entity.HasOne(p => p.ProductCategory)
                      .WithMany(pc => pc.Products)
                      .HasForeignKey(p => p.CategoryName)
                      .HasPrincipalKey(pc => pc.CategoryName);
            });

            modelBuilder.Entity<Specification>(entity =>
            {
                entity.HasKey(s => s.Id);
                entity.HasIndex(s => s.SpecificationId).IsUnique();
                entity.Property(s => s.SpecificationId).IsRequired();
                entity.Property(s => s.SpecificationBlankId).IsRequired();

                entity.HasOne(s => s.SpecificationBlank)
                    .WithMany(sb => sb.Specifications)
                    .HasForeignKey(s => s.SpecificationBlankId)
                    .HasPrincipalKey(sb => sb.SpecificationBlankId)
                    ;
            });

            modelBuilder.Entity<SpecificationBlank>(entity =>
            {
                entity.HasKey(sb => sb.Id);
                entity.HasIndex(sb => sb.SpecificationBlankId).IsUnique();
                entity.Property(sb => sb.SpecificationBlankId).IsRequired();
            });

            modelBuilder.Entity<ProductSpecificationBlank>(entity =>
            {
                entity.HasKey(psb => psb.Id);
                entity.Property(psb => psb.ProductSpecificationBlankId).IsRequired();
                entity.Property(psb => psb.SpecificationBlankId).IsRequired();
                entity.Property(psb => psb.ProductId).IsRequired();

                entity.HasOne(psb => psb.SpecificationBlank)
                    .WithMany(sb => sb.ProductSpecificationBlanks)
                    .HasForeignKey(psb => psb.SpecificationBlankId)
                    .HasPrincipalKey(sb => sb.SpecificationBlankId);

                entity.HasOne(psb => psb.Product)
                    .WithOne(p => p.ProductSpecificationBlank)
                    .HasForeignKey<ProductSpecificationBlank>(psb => psb.ProductId)
                    .HasPrincipalKey<Product>(p => p.ProductId);
            });

            modelBuilder.Entity<ProductSpecification>(entity =>
            {
                entity.HasKey(ps => ps.Id);
                entity.Property(ps => ps.ProductSpecificationBlankId).IsRequired();
                entity.Property(ps => ps.ProductSpecificationId).IsRequired();
                entity.HasIndex(ps => new { ps.ProductSpecificationId, ps.ProductSpecificationBlankId }).IsUnique();
                //entity.Property(p => p.ProductSpecificationValue).IsRequired();

                entity.HasOne(ps => ps.ProductSpecificationBlank)
                    .WithMany(psb => psb.ProductSpecifications)
                    .HasForeignKey(ps => ps.ProductSpecificationBlankId)
                    .HasPrincipalKey(psb => psb.ProductSpecificationBlankId);
            });

            modelBuilder.Entity<InventoryItemTransaction>(entity =>
            {
                entity.HasKey(it => it.Id);
                entity.Property(it => it.OperationDate).IsRequired();
                entity.Property(it => it.InventoryItemId).IsRequired();
                entity.Property(it => it.TransactionType).IsRequired();
                entity.Property(it => it.Quantity).IsRequired();
                entity.Property(it => it.OrderId).IsRequired();

                entity.HasOne(it => it.InventoryItem)
                    .WithMany(il => il.InventoryItemTransactions)
                    .HasForeignKey(it => it.InventoryItemId)
                    .HasPrincipalKey(ii => ii.InventoryItemId);
            });

            modelBuilder.Entity<InventoryItem>(entity =>
            {
                entity.HasKey(il => il.Id);
                entity.Property(il => il.InventoryItemId).IsRequired();
                entity.Property(il => il.ProductId).IsRequired();
                entity.Property(il => il.ResponsibleEmployeeId).IsRequired();
                entity.Property(il => il.EntryDate).IsRequired();
                entity.Property(il => il.Status).IsRequired();
                entity.Property(il => il.WorkplaceId).IsRequired();
                entity.Property(il => il.OrderRecordId).IsRequired();
                
                entity.HasOne(il => il.Product)
                    .WithMany(p => p.InventoryItems)
                    .HasForeignKey(il => il.ProductId)
                    .HasPrincipalKey(p => p.ProductId);

                entity.HasOne(il => il.Workplace)
                    .WithMany(w => w.InventoryItems)
                    .HasForeignKey(il => il.WorkplaceId)
                    .HasPrincipalKey(w => w.WorkplaceId);
            });

            modelBuilder.Entity<InventoryItemSpecification>(entity =>
            {
                entity.HasKey(iis => iis.Id);
                entity.HasIndex(iis => new { iis.InventoryItemId, iis.InventoryItemSpecificationId }).IsUnique();
                entity.Property(iis => iis.InventoryItemId).IsRequired();
                entity.Property(iis => iis.InventoryItemSpecificationId).IsRequired();
                
                entity.HasOne(iis => iis.InventoryItem)
                    .WithOne(il => il.InventoryItemSpecification)
                    .HasForeignKey<InventoryItemSpecification>(iis => iis.InventoryItemId)
                    .HasPrincipalKey<InventoryItem>(ii => ii.InventoryItemId);
            });

            modelBuilder.Entity<Distribution>(entity =>
            {
                entity.HasKey(d => d.Id);
                entity.HasIndex(d => d.DistributionId).IsUnique();
                entity.Property(d => d.DistributionId).IsRequired();
                entity.Property(d => d.DistributionDate).IsRequired();
                entity.Property(d => d.ResponsibleEmployeeId).IsRequired();
                entity.Property(d => d.ResponsibleStockEmployeeId).IsRequired();

                entity.HasOne(d => d.InventoryItemTransaction)
                    .WithOne(it => it.Distribution)
                    .HasForeignKey<Distribution>(d => d.InventoryItemTransactionId)
                    .HasPrincipalKey<InventoryItemTransaction>(d => d.Id);
            });

            modelBuilder.Entity<DistributionRecord>(entity =>
            {
                entity.HasKey(dr => dr.Id);
                entity.HasIndex(dr => new { dr.InventoryItemId, dr.DistributionId }).IsUnique();
                entity.Property(dr => dr.DistributionId).IsRequired();
                entity.Property(dr => dr.InventoryItemId).IsRequired();
                entity.Property(dr => dr.Quantity).IsRequired();
                entity.Property(dr => dr.WorkplaceId).IsRequired();

                entity.HasOne(dr => dr.Distribution)
                    .WithMany(d => d.DistributionRecords)
                    .HasForeignKey(dr => dr.DistributionId)
                    .HasPrincipalKey(d => d.DistributionId);

                entity.HasOne(dr => dr.InventoryItem)
                    .WithMany(il => il.DistributionRecords)
                    .HasForeignKey(dr => dr.InventoryItemId)
                    .HasPrincipalKey(ii => ii.InventoryItemId);

                entity.HasOne(dr => dr.Workplace)
                    .WithMany(w => w.DistributionRecords)
                    .HasForeignKey(dr => dr.WorkplaceId)
                    .HasPrincipalKey(w => w.WorkplaceId);
            });

            modelBuilder.Entity<AmortisationReason>(entity =>
            {
                entity.HasKey(ar => ar.Id);
                entity.HasIndex(ar => ar.Name).IsUnique();
                entity.Property(ar => ar.Name).IsRequired();
                entity.Property(ar => ar.ShortDescription).IsRequired();
            });

            modelBuilder.Entity<Amortisation>(entity =>
            {
                entity.HasKey(a => a.Id);
                entity.HasIndex(a => a.AmortisationId).IsUnique();
                entity.Property(a => a.AmortisationId).IsRequired();
                entity.Property(a => a.AmortisationDate).IsRequired();
                entity.Property(a => a.ResponsibleEmployeeId).IsRequired();
                
                entity.HasOne(a => a.InventoryItemTransaction)
                    .WithOne(it => it.Amortisation)
                    .HasForeignKey<Amortisation>(it => it.InventoryItemTransactionId)
                    .HasPrincipalKey<InventoryItemTransaction>(a => a.Id);
            });
            
            modelBuilder.Entity<AmortisationRecord>(entity =>
            {
                entity.HasKey(ar => ar.Id);
                entity.HasIndex(ar => new { ar.AmortisationId, ar.InventoryItemId }).IsUnique();
                entity.Property(ar => ar.AmortisationId).IsRequired();
                entity.Property(ar => ar.InventoryItemId).IsRequired();
                entity.Property(ar => ar.Quantity).IsRequired();
                entity.Property(ar => ar.AmortisationReasonId).IsRequired();

                entity.HasOne(arec => arec.AmortisationReason)
                    .WithMany(area => area.AmortisationRecords)
                    .HasForeignKey(arec => arec.AmortisationReasonId)
                    .HasPrincipalKey(area => area.Name);

                entity.HasOne(ar => ar.Amortisation)
                    .WithMany(a => a.AmortisationRecords)
                    .HasForeignKey(ar => ar.AmortisationId)
                    .HasPrincipalKey(a => a.AmortisationId);

                entity.HasOne(ar => ar.InventoryItem)
                    .WithMany(il => il.AmortisationRecords)
                    .HasForeignKey(ar => ar.InventoryItemId)
                    .HasPrincipalKey(ii => ii.InventoryItemId);
            });

            modelBuilder.Entity<Workplace>(entity =>
            {
                entity.HasKey(w => w.Id);
                entity.Property(w => w.BuildingId).IsRequired();
                entity.Property(w => w.RoomId).IsRequired();
                entity.Property(w => w.DepartmentId).IsRequired();
            });
        }
    }
}