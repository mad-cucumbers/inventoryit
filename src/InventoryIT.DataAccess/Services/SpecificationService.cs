﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.SpecificationModels;

namespace InventoryIT.DataAccess.Services
{
    public class SpecificationService : BaseService<SpecificationDTO, Specification>, ISpecificationService
    {
        public SpecificationService(ISpecificationRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }
    }
}
