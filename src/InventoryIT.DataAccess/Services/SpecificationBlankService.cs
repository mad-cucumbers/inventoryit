﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.SpecificationBlankModels;

namespace InventoryIT.DataAccess.Services
{
    public class SpecificationBlankService : BaseService<SpecificationBlankDTO, SpecificationBlank>, ISpecificationBlankService
    {
        public SpecificationBlankService(ISpecificationBlankRepository repository, IMapper mapper) :base(repository, mapper)
        {
        }
    }
}
