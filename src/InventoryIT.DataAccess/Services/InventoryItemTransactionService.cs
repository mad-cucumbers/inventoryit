﻿using AutoMapper;
using InventoryIT.Core.Abstractions;
using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.InventoryItemTransactionModels;
using InventoryIT.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Services
{
    public class InventoryItemTransactionService : BaseService<InventoryItemTransactionDTO, InventoryItemTransaction>, IInventoryItemTransactionService
    {
        public InventoryItemTransactionService(IInventoryItemTransactionRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }
    }
}
