﻿using AutoMapper;
using InventoryIT.Core.Abstractions;
using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.WorkPlaceModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Services
{
    public class WorkPlaceService : BaseService<WorkPlaceDTO, Workplace>, IWorkPlaceService
    {
        public WorkPlaceService(IWorkplaceRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }
    }
}
