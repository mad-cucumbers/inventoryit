﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.Enums;
using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Constants;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.AmortisationModels;
using InventoryIT.Core.ResponseModels;
using InventoryIT.DataAccess.Services;

namespace InventoryIT.DataAccess.Services
{
    public class AmortisationService : BaseService<AmortisationDTO, Amortisation>, IAmortisationService
    {
        public AmortisationService(IAmortisationRepository repository, IMapper mapper) : base(repository, mapper)
        {
        }
    }
}