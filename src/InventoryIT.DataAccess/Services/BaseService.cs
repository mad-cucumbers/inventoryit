﻿using AutoMapper;
using Contracts.Enums;
using InventoryIT.Core.Abstractions;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain;
using InventoryIT.Core.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryIT.DataAccess.Services
{
    public class BaseService<DTO,Entity> : IBaseService<DTO> where Entity:BaseEntity where DTO: BaseEntity
    {
        private readonly IGenericRepository<Entity> _repository;
        private readonly IMapper _mapper;

        public BaseService(IGenericRepository<Entity> repository, IMapper mapper)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<InventoryITActionResult> AddAsync(DTO model)
        {
            return await _repository.AddAsync(_mapper.Map<Entity>(model));

        }

        public async Task<InventoryITActionResult> DeleteAsync(int id)
        {
            var entity = await _repository.GetEntityByIdAsync(id);

            if (entity.AspNetException != null) return entity;
            if (entity.Data == null)
                return InventoryITActionResult.Fail(new[] { TypeOfErrors.NoContent },
                    $"Ошибка при удалении модели, данные не найдены || Модель < {typeof(Entity)} > || Входной параметр < {id} >");

            return await _repository.DeleteAsync(entity.Data);
        }

        public async Task<InventoryITActionResult<IEnumerable<DTO>>> GetAllAsync()
        {
            var specifications = await _repository.GetAllAsync();

            if (specifications.AspNetException != null)
                return InventoryITActionResult<IEnumerable<DTO>>.Fail(null, specifications.Errors, specifications.AspNetException);
            if (!specifications.Data.Any())
                return InventoryITActionResult<IEnumerable<DTO>>
                    .Fail(null, new[] { TypeOfErrors.NoContent }, $"Не найдено ниодной модели || Модель: < {typeof(DTO)} >");

            var response = _mapper.Map<IEnumerable<DTO>>(specifications.Data);
            return InventoryITActionResult<IEnumerable<DTO>>.IsSuccess(response);
        }

        public async Task<InventoryITActionResult<DTO>> GetByIdAsync(int id)
        {
            var specifications = await _repository.GetEntityByIdAsync(id);

            if (specifications.AspNetException != null)
                return InventoryITActionResult<DTO>.Fail(null, specifications.Errors, specifications.AspNetException);
            if (specifications.Data == null)
                return InventoryITActionResult<DTO>
                    .Fail(null, new[] { TypeOfErrors.NoContent }, $"Не найдена модель || Модель: < {typeof(DTO)} >");

            var response = _mapper.Map<DTO>(specifications.Data);
            return InventoryITActionResult<DTO>.IsSuccess(response);
        }

        public async Task<InventoryITActionResult> UpdateAsync(DTO model)
        {
            var entity = await _repository.GetEntityByIdAsync(model.Id);

            if (entity.AspNetException != null) return entity;
            if (entity.Data == null)
                return InventoryITActionResult.Fail(new[] { TypeOfErrors.NoContent },
                    $"Ошибка при изменении модели, данные не найдены || Модель < {typeof(Entity)} > || Входной параметр < {model.Id} >");

            return await _repository.UpdateAsync(_mapper.Map<Entity>(model));
        }
    }
}
