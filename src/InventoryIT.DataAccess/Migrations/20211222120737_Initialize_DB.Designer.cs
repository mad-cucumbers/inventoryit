﻿// <auto-generated />
using System;
using InventoryIT.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace InventoryIT.DataAccess.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20211222120737_Initialize_DB")]
    partial class Initialize_DB
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.11")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Amortisation", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("AmortisationDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("AmortisationId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("InventoryItemTransactionId")
                        .HasColumnType("integer");

                    b.Property<int>("ResponsibleEmployeeId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("AmortisationId")
                        .IsUnique();

                    b.HasIndex("InventoryItemTransactionId")
                        .IsUnique();

                    b.ToTable("Amortisations");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.AmortisationReason", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ShortDescription")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("AmortisationReasons");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.AmortisationRecord", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("AmortisationId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("AmortisationReasonId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("InventoryItemId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<float>("Quantity")
                        .HasColumnType("real");

                    b.HasKey("Id");

                    b.HasIndex("AmortisationReasonId");

                    b.HasIndex("InventoryItemId");

                    b.HasIndex("AmortisationId", "InventoryItemId")
                        .IsUnique();

                    b.ToTable("AmortisationRecords");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Distribution", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("DistributionDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("DistributionId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("InventoryItemTransactionId")
                        .HasColumnType("integer");

                    b.Property<int>("ResponsibleEmployeeId")
                        .HasColumnType("integer");

                    b.Property<int>("ResponsibleStockEmployeeId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("DistributionId")
                        .IsUnique();

                    b.HasIndex("InventoryItemTransactionId")
                        .IsUnique();

                    b.ToTable("Distributions");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.DistributionRecord", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("DistributionId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("InventoryItemId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<float>("Quantity")
                        .HasColumnType("real");

                    b.Property<int>("WorkplaceId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("DistributionId");

                    b.HasIndex("WorkplaceId");

                    b.HasIndex("InventoryItemId", "DistributionId")
                        .IsUnique();

                    b.ToTable("DistributionRecords");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.InventoryItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("EntryDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("InventoryItemId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("OrderRecordId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ProductId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<float>("Quantity")
                        .HasColumnType("real");

                    b.Property<int>("ResponsibleEmployeeId")
                        .HasColumnType("integer");

                    b.Property<int>("Status")
                        .HasColumnType("integer");

                    b.Property<int>("WorkplaceId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("OrderRecordId")
                        .IsUnique();

                    b.HasIndex("ProductId");

                    b.HasIndex("WorkplaceId");

                    b.ToTable("InventoryItems");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.InventoryItemSpecification", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("InventoryItemId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("InventoryItemSpecificationId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("InventoryItemSpecificationValue")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("InventoryItemId")
                        .IsUnique();

                    b.HasIndex("InventoryItemId", "InventoryItemSpecificationId")
                        .IsUnique();

                    b.ToTable("InventoryItemSpecifications");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.InventoryItemTransaction", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("InventoryItemId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTime>("OperationDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("OrderId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<float>("Quantity")
                        .HasColumnType("real");

                    b.Property<int>("TransactionType")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("InventoryItemId");

                    b.HasIndex("OrderId")
                        .IsUnique();

                    b.ToTable("InventoryItemTransactions");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Comment")
                        .HasColumnType("text");

                    b.Property<string>("DepartmentId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("EmployeeId")
                        .HasColumnType("integer");

                    b.Property<DateTime>("OrderDate")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("OrderId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("OrderId")
                        .IsUnique();

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.OrderRecord", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("OrderId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("OrderRecordId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ProductId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<float>("Quantity")
                        .HasColumnType("real");

                    b.Property<string>("SerialNumber")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.ToTable("OrderRecords");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("CategoryName")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("FullName")
                        .HasColumnType("text");

                    b.Property<string>("ProductId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ShortName")
                        .HasColumnType("text");

                    b.Property<string>("Vendor")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("CategoryName");

                    b.HasIndex("ProductId")
                        .IsUnique();

                    b.ToTable("Products");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.ProductCategory", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("CategoryName")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Description")
                        .HasColumnType("text");

                    b.Property<bool>("IsBatch")
                        .HasColumnType("boolean");

                    b.Property<bool>("IsSpecRequired")
                        .HasColumnType("boolean");

                    b.HasKey("Id");

                    b.HasIndex("CategoryName")
                        .IsUnique();

                    b.ToTable("ProductCategories");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.ProductSpecification", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("ProductSpecificationBlankId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ProductSpecificationId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ProductSpecificationValue")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("ProductSpecificationBlankId");

                    b.HasIndex("ProductSpecificationId", "ProductSpecificationBlankId")
                        .IsUnique();

                    b.ToTable("ProductSpecifications");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.ProductSpecificationBlank", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("ProductId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("ProductSpecificationBlankId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("SpecificationBlankId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("ProductId")
                        .IsUnique();

                    b.HasIndex("SpecificationBlankId");

                    b.ToTable("ProductSpecificationBlanks");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Specification", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<bool>("IsRequered")
                        .HasColumnType("boolean");

                    b.Property<string>("SpecificationBlankId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("SpecificationId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("SpecificationBlankId");

                    b.HasIndex("SpecificationId")
                        .IsUnique();

                    b.ToTable("Specifications");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.SpecificationBlank", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("SpecificationBlankId")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("SpecificationBlankId")
                        .IsUnique();

                    b.ToTable("SpecificationBlanks");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Workplace", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int>("BuildingId")
                        .HasColumnType("integer");

                    b.Property<int>("DepartmentId")
                        .HasColumnType("integer");

                    b.Property<int>("RoomId")
                        .HasColumnType("integer");

                    b.Property<int>("WorkplaceId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.ToTable("Workplaces");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Amortisation", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.InventoryItemTransaction", "InventoryItemTransaction")
                        .WithOne("Amortisation")
                        .HasForeignKey("InventoryIT.Core.Domain.Entities.Amortisation", "InventoryItemTransactionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("InventoryItemTransaction");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.AmortisationRecord", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.Amortisation", "Amortisation")
                        .WithMany("AmortisationRecords")
                        .HasForeignKey("AmortisationId")
                        .HasPrincipalKey("AmortisationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("InventoryIT.Core.Domain.Entities.AmortisationReason", "AmortisationReason")
                        .WithMany("AmortisationRecords")
                        .HasForeignKey("AmortisationReasonId")
                        .HasPrincipalKey("Name")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("InventoryIT.Core.Domain.Entities.InventoryItem", "InventoryItem")
                        .WithMany("AmortisationRecords")
                        .HasForeignKey("InventoryItemId")
                        .HasPrincipalKey("InventoryItemId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Amortisation");

                    b.Navigation("AmortisationReason");

                    b.Navigation("InventoryItem");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Distribution", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.InventoryItemTransaction", "InventoryItemTransaction")
                        .WithOne("Distribution")
                        .HasForeignKey("InventoryIT.Core.Domain.Entities.Distribution", "InventoryItemTransactionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("InventoryItemTransaction");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.DistributionRecord", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.Distribution", "Distribution")
                        .WithMany("DistributionRecords")
                        .HasForeignKey("DistributionId")
                        .HasPrincipalKey("DistributionId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("InventoryIT.Core.Domain.Entities.InventoryItem", "InventoryItem")
                        .WithMany("DistributionRecords")
                        .HasForeignKey("InventoryItemId")
                        .HasPrincipalKey("InventoryItemId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("InventoryIT.Core.Domain.Entities.Workplace", "Workplace")
                        .WithMany("DistributionRecords")
                        .HasForeignKey("WorkplaceId")
                        .HasPrincipalKey("WorkplaceId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Distribution");

                    b.Navigation("InventoryItem");

                    b.Navigation("Workplace");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.InventoryItem", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.OrderRecord", "OrderRecord")
                        .WithOne("InventoryItem")
                        .HasForeignKey("InventoryIT.Core.Domain.Entities.InventoryItem", "OrderRecordId")
                        .HasPrincipalKey("InventoryIT.Core.Domain.Entities.OrderRecord", "OrderRecordId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("InventoryIT.Core.Domain.Entities.Product", "Product")
                        .WithMany("InventoryItems")
                        .HasForeignKey("ProductId")
                        .HasPrincipalKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("InventoryIT.Core.Domain.Entities.Workplace", "Workplace")
                        .WithMany("InventoryItems")
                        .HasForeignKey("WorkplaceId")
                        .HasPrincipalKey("WorkplaceId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("OrderRecord");

                    b.Navigation("Product");

                    b.Navigation("Workplace");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.InventoryItemSpecification", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.InventoryItem", "InventoryItem")
                        .WithOne("InventoryItemSpecification")
                        .HasForeignKey("InventoryIT.Core.Domain.Entities.InventoryItemSpecification", "InventoryItemId")
                        .HasPrincipalKey("InventoryIT.Core.Domain.Entities.InventoryItem", "InventoryItemId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("InventoryItem");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.InventoryItemTransaction", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.InventoryItem", "InventoryItem")
                        .WithMany("InventoryItemTransactions")
                        .HasForeignKey("InventoryItemId")
                        .HasPrincipalKey("InventoryItemId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("InventoryIT.Core.Domain.Entities.Order", "Order")
                        .WithOne("InventoryItemTransaction")
                        .HasForeignKey("InventoryIT.Core.Domain.Entities.InventoryItemTransaction", "OrderId")
                        .HasPrincipalKey("InventoryIT.Core.Domain.Entities.Order", "OrderId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("InventoryItem");

                    b.Navigation("Order");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.OrderRecord", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.Order", "Order")
                        .WithMany("OrderRecords")
                        .HasForeignKey("OrderId")
                        .HasPrincipalKey("OrderId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("InventoryIT.Core.Domain.Entities.Product", "Product")
                        .WithMany("OrderRecords")
                        .HasForeignKey("OrderId")
                        .HasPrincipalKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Order");

                    b.Navigation("Product");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Product", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.ProductCategory", "ProductCategory")
                        .WithMany("Products")
                        .HasForeignKey("CategoryName")
                        .HasPrincipalKey("CategoryName")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ProductCategory");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.ProductSpecification", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.ProductSpecificationBlank", "ProductSpecificationBlank")
                        .WithMany("ProductSpecifications")
                        .HasForeignKey("ProductSpecificationBlankId")
                        .HasPrincipalKey("ProductSpecificationBlankId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ProductSpecificationBlank");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.ProductSpecificationBlank", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.Product", "Product")
                        .WithOne("ProductSpecificationBlank")
                        .HasForeignKey("InventoryIT.Core.Domain.Entities.ProductSpecificationBlank", "ProductId")
                        .HasPrincipalKey("InventoryIT.Core.Domain.Entities.Product", "ProductId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("InventoryIT.Core.Domain.Entities.SpecificationBlank", "SpecificationBlank")
                        .WithMany("ProductSpecificationBlanks")
                        .HasForeignKey("SpecificationBlankId")
                        .HasPrincipalKey("SpecificationBlankId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Product");

                    b.Navigation("SpecificationBlank");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Specification", b =>
                {
                    b.HasOne("InventoryIT.Core.Domain.Entities.SpecificationBlank", "SpecificationBlank")
                        .WithMany("Specifications")
                        .HasForeignKey("SpecificationBlankId")
                        .HasPrincipalKey("SpecificationBlankId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("SpecificationBlank");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Amortisation", b =>
                {
                    b.Navigation("AmortisationRecords");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.AmortisationReason", b =>
                {
                    b.Navigation("AmortisationRecords");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Distribution", b =>
                {
                    b.Navigation("DistributionRecords");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.InventoryItem", b =>
                {
                    b.Navigation("AmortisationRecords");

                    b.Navigation("DistributionRecords");

                    b.Navigation("InventoryItemSpecification");

                    b.Navigation("InventoryItemTransactions");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.InventoryItemTransaction", b =>
                {
                    b.Navigation("Amortisation");

                    b.Navigation("Distribution");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Order", b =>
                {
                    b.Navigation("InventoryItemTransaction");

                    b.Navigation("OrderRecords");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.OrderRecord", b =>
                {
                    b.Navigation("InventoryItem");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Product", b =>
                {
                    b.Navigation("InventoryItems");

                    b.Navigation("OrderRecords");

                    b.Navigation("ProductSpecificationBlank");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.ProductCategory", b =>
                {
                    b.Navigation("Products");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.ProductSpecificationBlank", b =>
                {
                    b.Navigation("ProductSpecifications");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.SpecificationBlank", b =>
                {
                    b.Navigation("ProductSpecificationBlanks");

                    b.Navigation("Specifications");
                });

            modelBuilder.Entity("InventoryIT.Core.Domain.Entities.Workplace", b =>
                {
                    b.Navigation("DistributionRecords");

                    b.Navigation("InventoryItems");
                });
#pragma warning restore 612, 618
        }
    }
}
