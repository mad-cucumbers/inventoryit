﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace InventoryIT.DataAccess.Migrations
{
    public partial class Initialize_DB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AmortisationReasons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    ShortDescription = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AmortisationReasons", x => x.Id);
                    table.UniqueConstraint("AK_AmortisationReasons_Name", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OrderId = table.Column<string>(type: "text", nullable: false),
                    OrderDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Comment = table.Column<string>(type: "text", nullable: true),
                    DepartmentId = table.Column<string>(type: "text", nullable: false),
                    EmployeeId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.UniqueConstraint("AK_Orders_OrderId", x => x.OrderId);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CategoryName = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    IsBatch = table.Column<bool>(type: "boolean", nullable: false),
                    IsSpecRequired = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategories", x => x.Id);
                    table.UniqueConstraint("AK_ProductCategories_CategoryName", x => x.CategoryName);
                });

            migrationBuilder.CreateTable(
                name: "SpecificationBlanks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SpecificationBlankId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpecificationBlanks", x => x.Id);
                    table.UniqueConstraint("AK_SpecificationBlanks_SpecificationBlankId", x => x.SpecificationBlankId);
                });

            migrationBuilder.CreateTable(
                name: "Workplaces",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    WorkplaceId = table.Column<int>(type: "integer", nullable: false),
                    BuildingId = table.Column<int>(type: "integer", nullable: false),
                    RoomId = table.Column<int>(type: "integer", nullable: false),
                    DepartmentId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workplaces", x => x.Id);
                    table.UniqueConstraint("AK_Workplaces_WorkplaceId", x => x.WorkplaceId);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ProductId = table.Column<string>(type: "text", nullable: false),
                    FullName = table.Column<string>(type: "text", nullable: true),
                    ShortName = table.Column<string>(type: "text", nullable: true),
                    CategoryName = table.Column<string>(type: "text", nullable: false),
                    Vendor = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.UniqueConstraint("AK_Products_ProductId", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_Products_ProductCategories_CategoryName",
                        column: x => x.CategoryName,
                        principalTable: "ProductCategories",
                        principalColumn: "CategoryName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Specifications",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SpecificationId = table.Column<string>(type: "text", nullable: false),
                    IsRequered = table.Column<bool>(type: "boolean", nullable: false),
                    SpecificationBlankId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Specifications_SpecificationBlanks_SpecificationBlankId",
                        column: x => x.SpecificationBlankId,
                        principalTable: "SpecificationBlanks",
                        principalColumn: "SpecificationBlankId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderRecords",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OrderRecordId = table.Column<string>(type: "text", nullable: false),
                    OrderId = table.Column<string>(type: "text", nullable: false),
                    ProductId = table.Column<string>(type: "text", nullable: false),
                    Quantity = table.Column<float>(type: "real", nullable: false),
                    SerialNumber = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderRecords", x => x.Id);
                    table.UniqueConstraint("AK_OrderRecords_OrderRecordId", x => x.OrderRecordId);
                    table.ForeignKey(
                        name: "FK_OrderRecords_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "OrderId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderRecords_Products_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductSpecificationBlanks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ProductSpecificationBlankId = table.Column<string>(type: "text", nullable: false),
                    SpecificationBlankId = table.Column<string>(type: "text", nullable: false),
                    ProductId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductSpecificationBlanks", x => x.Id);
                    table.UniqueConstraint("AK_ProductSpecificationBlanks_ProductSpecificationBlankId", x => x.ProductSpecificationBlankId);
                    table.ForeignKey(
                        name: "FK_ProductSpecificationBlanks_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductSpecificationBlanks_SpecificationBlanks_Specificatio~",
                        column: x => x.SpecificationBlankId,
                        principalTable: "SpecificationBlanks",
                        principalColumn: "SpecificationBlankId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InventoryItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    InventoryItemId = table.Column<string>(type: "text", nullable: false),
                    OrderRecordId = table.Column<string>(type: "text", nullable: false),
                    Quantity = table.Column<float>(type: "real", nullable: false),
                    ProductId = table.Column<string>(type: "text", nullable: false),
                    EntryDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ResponsibleEmployeeId = table.Column<int>(type: "integer", nullable: false),
                    WorkplaceId = table.Column<int>(type: "integer", nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryItems", x => x.Id);
                    table.UniqueConstraint("AK_InventoryItems_InventoryItemId", x => x.InventoryItemId);
                    table.ForeignKey(
                        name: "FK_InventoryItems_OrderRecords_OrderRecordId",
                        column: x => x.OrderRecordId,
                        principalTable: "OrderRecords",
                        principalColumn: "OrderRecordId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InventoryItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InventoryItems_Workplaces_WorkplaceId",
                        column: x => x.WorkplaceId,
                        principalTable: "Workplaces",
                        principalColumn: "WorkplaceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductSpecifications",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ProductSpecificationId = table.Column<string>(type: "text", nullable: false),
                    ProductSpecificationBlankId = table.Column<string>(type: "text", nullable: false),
                    ProductSpecificationValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductSpecifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductSpecifications_ProductSpecificationBlanks_ProductSpe~",
                        column: x => x.ProductSpecificationBlankId,
                        principalTable: "ProductSpecificationBlanks",
                        principalColumn: "ProductSpecificationBlankId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InventoryItemSpecifications",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    InventoryItemSpecificationId = table.Column<string>(type: "text", nullable: false),
                    InventoryItemId = table.Column<string>(type: "text", nullable: false),
                    InventoryItemSpecificationValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryItemSpecifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InventoryItemSpecifications_InventoryItems_InventoryItemId",
                        column: x => x.InventoryItemId,
                        principalTable: "InventoryItems",
                        principalColumn: "InventoryItemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InventoryItemTransactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OperationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    InventoryItemId = table.Column<string>(type: "text", nullable: false),
                    OrderId = table.Column<string>(type: "text", nullable: false),
                    TransactionType = table.Column<int>(type: "integer", nullable: false),
                    Quantity = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryItemTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InventoryItemTransactions_InventoryItems_InventoryItemId",
                        column: x => x.InventoryItemId,
                        principalTable: "InventoryItems",
                        principalColumn: "InventoryItemId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InventoryItemTransactions_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "OrderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Amortisations",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AmortisationId = table.Column<string>(type: "text", nullable: false),
                    InventoryItemTransactionId = table.Column<int>(type: "integer", nullable: false),
                    AmortisationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ResponsibleEmployeeId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Amortisations", x => x.Id);
                    table.UniqueConstraint("AK_Amortisations_AmortisationId", x => x.AmortisationId);
                    table.ForeignKey(
                        name: "FK_Amortisations_InventoryItemTransactions_InventoryItemTransa~",
                        column: x => x.InventoryItemTransactionId,
                        principalTable: "InventoryItemTransactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Distributions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DistributionId = table.Column<string>(type: "text", nullable: false),
                    DistributionDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ResponsibleEmployeeId = table.Column<int>(type: "integer", nullable: false),
                    ResponsibleStockEmployeeId = table.Column<int>(type: "integer", nullable: false),
                    InventoryItemTransactionId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Distributions", x => x.Id);
                    table.UniqueConstraint("AK_Distributions_DistributionId", x => x.DistributionId);
                    table.ForeignKey(
                        name: "FK_Distributions_InventoryItemTransactions_InventoryItemTransa~",
                        column: x => x.InventoryItemTransactionId,
                        principalTable: "InventoryItemTransactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AmortisationRecords",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AmortisationId = table.Column<string>(type: "text", nullable: false),
                    InventoryItemId = table.Column<string>(type: "text", nullable: false),
                    Quantity = table.Column<float>(type: "real", nullable: false),
                    AmortisationReasonId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AmortisationRecords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AmortisationRecords_AmortisationReasons_AmortisationReasonId",
                        column: x => x.AmortisationReasonId,
                        principalTable: "AmortisationReasons",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AmortisationRecords_Amortisations_AmortisationId",
                        column: x => x.AmortisationId,
                        principalTable: "Amortisations",
                        principalColumn: "AmortisationId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AmortisationRecords_InventoryItems_InventoryItemId",
                        column: x => x.InventoryItemId,
                        principalTable: "InventoryItems",
                        principalColumn: "InventoryItemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DistributionRecords",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DistributionId = table.Column<string>(type: "text", nullable: false),
                    InventoryItemId = table.Column<string>(type: "text", nullable: false),
                    WorkplaceId = table.Column<int>(type: "integer", nullable: false),
                    Quantity = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistributionRecords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DistributionRecords_Distributions_DistributionId",
                        column: x => x.DistributionId,
                        principalTable: "Distributions",
                        principalColumn: "DistributionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DistributionRecords_InventoryItems_InventoryItemId",
                        column: x => x.InventoryItemId,
                        principalTable: "InventoryItems",
                        principalColumn: "InventoryItemId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DistributionRecords_Workplaces_WorkplaceId",
                        column: x => x.WorkplaceId,
                        principalTable: "Workplaces",
                        principalColumn: "WorkplaceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AmortisationReasons_Name",
                table: "AmortisationReasons",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AmortisationRecords_AmortisationId_InventoryItemId",
                table: "AmortisationRecords",
                columns: new[] { "AmortisationId", "InventoryItemId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AmortisationRecords_AmortisationReasonId",
                table: "AmortisationRecords",
                column: "AmortisationReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_AmortisationRecords_InventoryItemId",
                table: "AmortisationRecords",
                column: "InventoryItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Amortisations_AmortisationId",
                table: "Amortisations",
                column: "AmortisationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Amortisations_InventoryItemTransactionId",
                table: "Amortisations",
                column: "InventoryItemTransactionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DistributionRecords_DistributionId",
                table: "DistributionRecords",
                column: "DistributionId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributionRecords_InventoryItemId_DistributionId",
                table: "DistributionRecords",
                columns: new[] { "InventoryItemId", "DistributionId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DistributionRecords_WorkplaceId",
                table: "DistributionRecords",
                column: "WorkplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Distributions_DistributionId",
                table: "Distributions",
                column: "DistributionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Distributions_InventoryItemTransactionId",
                table: "Distributions",
                column: "InventoryItemTransactionId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItems_OrderRecordId",
                table: "InventoryItems",
                column: "OrderRecordId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItems_ProductId",
                table: "InventoryItems",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItems_WorkplaceId",
                table: "InventoryItems",
                column: "WorkplaceId");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItemSpecifications_InventoryItemId",
                table: "InventoryItemSpecifications",
                column: "InventoryItemId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItemSpecifications_InventoryItemId_InventoryItemSp~",
                table: "InventoryItemSpecifications",
                columns: new[] { "InventoryItemId", "InventoryItemSpecificationId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItemTransactions_InventoryItemId",
                table: "InventoryItemTransactions",
                column: "InventoryItemId");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryItemTransactions_OrderId",
                table: "InventoryItemTransactions",
                column: "OrderId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrderRecords_OrderId",
                table: "OrderRecords",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrderId",
                table: "Orders",
                column: "OrderId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductCategories_CategoryName",
                table: "ProductCategories",
                column: "CategoryName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryName",
                table: "Products",
                column: "CategoryName");

            migrationBuilder.CreateIndex(
                name: "IX_Products_ProductId",
                table: "Products",
                column: "ProductId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductSpecificationBlanks_ProductId",
                table: "ProductSpecificationBlanks",
                column: "ProductId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductSpecificationBlanks_SpecificationBlankId",
                table: "ProductSpecificationBlanks",
                column: "SpecificationBlankId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSpecifications_ProductSpecificationBlankId",
                table: "ProductSpecifications",
                column: "ProductSpecificationBlankId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductSpecifications_ProductSpecificationId_ProductSpecifi~",
                table: "ProductSpecifications",
                columns: new[] { "ProductSpecificationId", "ProductSpecificationBlankId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SpecificationBlanks_SpecificationBlankId",
                table: "SpecificationBlanks",
                column: "SpecificationBlankId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Specifications_SpecificationBlankId",
                table: "Specifications",
                column: "SpecificationBlankId");

            migrationBuilder.CreateIndex(
                name: "IX_Specifications_SpecificationId",
                table: "Specifications",
                column: "SpecificationId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AmortisationRecords");

            migrationBuilder.DropTable(
                name: "DistributionRecords");

            migrationBuilder.DropTable(
                name: "InventoryItemSpecifications");

            migrationBuilder.DropTable(
                name: "ProductSpecifications");

            migrationBuilder.DropTable(
                name: "Specifications");

            migrationBuilder.DropTable(
                name: "AmortisationReasons");

            migrationBuilder.DropTable(
                name: "Amortisations");

            migrationBuilder.DropTable(
                name: "Distributions");

            migrationBuilder.DropTable(
                name: "ProductSpecificationBlanks");

            migrationBuilder.DropTable(
                name: "InventoryItemTransactions");

            migrationBuilder.DropTable(
                name: "SpecificationBlanks");

            migrationBuilder.DropTable(
                name: "InventoryItems");

            migrationBuilder.DropTable(
                name: "OrderRecords");

            migrationBuilder.DropTable(
                name: "Workplaces");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "ProductCategories");
        }
    }
}
