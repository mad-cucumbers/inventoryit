﻿using AutoMapper;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.AmortisationModels;
using InventoryIT.Core.Models.AmortisationReasonModels;
using InventoryIT.Core.Models.AmortisationRecordModels;
using InventoryIT.Core.Models.DistributionModels;
using InventoryIT.Core.Models.DistributionRecordModels;
using InventoryIT.Core.Models.InventoryItemModels;
using InventoryIT.Core.Models.InventoryItemSpecificationModels;
using InventoryIT.Core.Models.InventoryItemTransactionModels;
using InventoryIT.Core.Models.OrderModels;
using InventoryIT.Core.Models.OrderRecordModels;
using InventoryIT.Core.Models.ProductModels;
using InventoryIT.Core.Models.ProductCategoryModels;
using InventoryIT.Core.Models.ProductSpecificationBlankModels;
using InventoryIT.Core.Models.ProductSpecificationModels;
using InventoryIT.Core.Models.SpecificationBlankModels;
using InventoryIT.Core.Models.SpecificationModels;
using InventoryIT.Core.Models.WorkPlaceModels;
using InventoryIT.WebHost.Models;

namespace InventoryIT.DataAccess.Mapper
{
    public class InventoryMapper : Profile
    {
        public InventoryMapper()
        {

            #region Amortisation

            CreateMap<Amortisation, AmortisationDTO>()
                //.ForMember(dest => dest.Id, x => x.MapFrom(src => src.Id))
                .ForMember(dest => dest.AmortisationDate, x => x.MapFrom(src => src.AmortisationDate))
                .ForMember(dest => dest.AmortisationId, x => x.MapFrom(src => src.AmortisationId))
                .ForMember(dest => dest.InventoryItemTransaction, x => x.MapFrom(src => src.InventoryItemTransaction))
                .ForMember(dest => dest.ResponsibleEmployeeId, x => x.MapFrom(src => src.ResponsibleEmployeeId))
                .ForMember(dest => dest.InventoryItemTransactionId, x => x.MapFrom(src => src.InventoryItemTransactionId))
                .ReverseMap()
                //.ForMember(dest => dest.Id, x => x.MapFrom(src => src.Id))
                .ForMember(dest => dest.AmortisationId, x => x.MapFrom(src => src.AmortisationId))
                .ForMember(dest => dest.AmortisationDate, x => x.MapFrom(src => src.AmortisationDate))
                .ForMember(dest => dest.InventoryItemTransaction, x => x.MapFrom(src => src.InventoryItemTransaction))
                .ForMember(dest => dest.ResponsibleEmployeeId, x => x.MapFrom(src => src.ResponsibleEmployeeId))
                .ForMember(dest => dest.InventoryItemTransactionId, x => x.MapFrom(src => src.InventoryItemTransactionId));

            #endregion

            #region AmortisationReason

            CreateMap<AmortisationReason, AmortisationReasonDTO>()
                .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, d => d.MapFrom(src => src.Name))
                .ForMember(dest => dest.ShortDescription, d => d.MapFrom(src => src.ShortDescription))
                .ReverseMap()
                .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, d => d.MapFrom(src => src.Name))
                .ForMember(dest => dest.ShortDescription, d => d.MapFrom(src => src.ShortDescription));

            #endregion

            #region AmortisationRecord

            CreateMap<AmortisationRecord, AmortisationRecordDTO>()
                .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
                .ForMember(dest => dest.AmortisationId, d => d.MapFrom(src => src.Amortisation.Id))
                .ForMember(dest => dest.InventoryItem, d => d.MapFrom(src => src.InventoryItem))
                .ForMember(dest => dest.Quantity, d => d.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.AmortisationReason, d => d.MapFrom(src => src.AmortisationReason))
                .ForMember(dest => dest.Amortisation, d => d.MapFrom(src => src.Amortisation))
                .ForMember(dest => dest.AmortisationReasonId, d => d.MapFrom(src => src.AmortisationReason.Id))
                .ForMember(dest => dest.InventoryItemId, d => d.MapFrom(src => src.InventoryItem.Id))
                .ReverseMap()
                .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
                .ForMember(dest => dest.InventoryItemId, d => d.MapFrom(src => src.InventoryItem.InventoryItemId))
                .ForMember(dest => dest.AmortisationReasonId, d => d.MapFrom(src => src.AmortisationReason.AmortisationReasonId))
                .ForMember(dest => dest.AmortisationId, d => d.MapFrom(src => src.Amortisation.AmortisationId))
                .ForMember(dest => dest.Amortisation, d => d.MapFrom(src => src.Amortisation))
                .ForMember(dest => dest.AmortisationReason, d => d.MapFrom(src => src.AmortisationReason))
                .ForMember(dest => dest.InventoryItem, d => d.MapFrom(src => src.InventoryItem))
                .ForMember(dest => dest.Quantity, d => d.MapFrom(src => src.Quantity));

            #endregion

            #region InventoryItem

            CreateMap<InventoryItem, InventoryItemDTO>()
                .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
                .ForMember(dest => dest.InventoryItemId, d => d.MapFrom(src => src.InventoryItemId))
                .ForMember(dest => dest.OrderRecord, d => d.MapFrom(src => src.OrderRecord))
                .ForMember(dest => dest.OrderRecordId, d => d.MapFrom(src => src.OrderRecordId))
                .ForMember(dest => dest.Workplace, d => d.MapFrom(src => src.Workplace))
                .ForMember(dest => dest.WorkplaceId, d => d.MapFrom(src => src.WorkplaceId))
                .ForMember(dest => dest.Quantity, d => d.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.Product, d => d.MapFrom(src => src.Product))
                .ForMember(dest => dest.ProductId, d => d.MapFrom(src => src.ProductId))
                .ForMember(dest => dest.EntryDate, d => d.MapFrom(src => src.EntryDate))
                .ForMember(dest => dest.Status, d => d.MapFrom(src => src.Status))
                .ForMember(dest => dest.InventoryItemSpecification, d => d.MapFrom(src => src.InventoryItemSpecification))
                .ForMember(dest => dest.InventoryItemSpecificationId, d => d.MapFrom(src => src.InventoryItemSpecification.InventoryItemSpecificationId))
                .ForMember(dest => dest.EmployeeId, d => d.MapFrom(src => src.ResponsibleEmployeeId))
                .ReverseMap()
                .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
                .ForMember(dest => dest.ResponsibleEmployeeId, d => d.MapFrom(src => src.EmployeeId))
                .ForMember(dest => dest.EntryDate, d => d.MapFrom(src => src.EntryDate))
                .ForMember(dest => dest.InventoryItemId, d => d.MapFrom(src => src.InventoryItemId))
                .ForMember(dest => dest.InventoryItemSpecification, d => d.MapFrom(src => src.InventoryItemSpecification))                
                .ForMember(dest => dest.OrderRecord, d => d.MapFrom(src => src.OrderRecord))
                .ForMember(dest => dest.OrderRecordId, d => d.MapFrom(src => src.OrderRecordId))
                .ForMember(dest => dest.Product, d => d.MapFrom(src => src.Product))
                .ForMember(dest => dest.ProductId, d => d.MapFrom(src => src.ProductId))
                .ForMember(dest => dest.Quantity, d => d.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.Status, d => d.MapFrom(src => src.Status))
                .ForMember(dest => dest.Workplace, d => d.MapFrom(src => src.Workplace))
                .ForMember(dest => dest.WorkplaceId, d => d.MapFrom(src => src.WorkplaceId));

            #endregion

            #region InventoryItemTransaction

            CreateMap<InventoryItemTransaction, InventoryItemTransactionDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(src => src.Id))
                .ForMember(dest => dest.AmortisationId, x => x.MapFrom(src => src.Amortisation.AmortisationId))
                .ForMember(dest => dest.Amortisation, x => x.MapFrom(src => src.Amortisation))
                .ForMember(dest => dest.DistributionId, x => x.MapFrom(src => src.Distribution.DistributionId))
                .ForMember(dest => dest.Distribution, x => x.MapFrom(src => src.Distribution))
                .ForMember(dest => dest.InventoryItemId, x => x.MapFrom(src => src.InventoryItem.InventoryItemId))
                .ForMember(dest => dest.InventoryItem, x => x.MapFrom(src => src.InventoryItem))
                .ForMember(dest => dest.OperationDate, x => x.MapFrom(src => src.OperationDate))
                .ForMember(dest => dest.OrderId, x => x.MapFrom(src => src.Order.Id))
                .ForMember(dest => dest.Order, x => x.MapFrom(src => src.Order))
                .ForMember(dest => dest.Quantity, x => x.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.TransactionType, x => x.MapFrom(src => src.TransactionType))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(src => src.Id))
                .ForMember(dest => dest.Amortisation, x => x.MapFrom(src => src.Amortisation))
                .ForMember(dest => dest.Distribution, x => x.MapFrom(src => src.Distribution))
                .ForMember(dest => dest.InventoryItem, x => x.MapFrom(src => src.InventoryItem))
                .ForMember(dest => dest.InventoryItemId, x => x.MapFrom(src => src.InventoryItem.InventoryItemId))
                .ForMember(dest => dest.Order, x => x.MapFrom(src => src.Order))
                .ForMember(dest => dest.OrderId, x => x.MapFrom(src => src.Order.OrderId))
                .ForMember(dest => dest.OperationDate, x => x.MapFrom(src => src.OperationDate))
                .ForMember(dest => dest.Quantity, x => x.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.TransactionType, x => x.MapFrom(src => src.TransactionType));

            #endregion

            #region InventoryItemSpecification

            CreateMap<InventoryItemSpecification, InventoryItemSpecificationDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.InventoryItemSpecificationId, x => x.MapFrom(dest => dest.InventoryItemSpecificationId))
                .ForMember(dest => dest.InventoryItem, x => x.MapFrom(dest => dest.InventoryItem))
                .ForMember(dest => dest.InventoryItemId, x => x.MapFrom(dest => dest.InventoryItemId))
                .ForMember(dest => dest.InventoryItemSpecificationValue, x => x.MapFrom(dest => dest.InventoryItemSpecificationValue))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.InventoryItemSpecificationId, x => x.MapFrom(dest => dest.InventoryItemSpecificationId))
                .ForMember(dest => dest.InventoryItem, x => x.MapFrom(dest => dest.InventoryItem))
                .ForMember(dest => dest.InventoryItemId, x => x.MapFrom(dest => dest.InventoryItem.InventoryItemId))
                .ForMember(dest => dest.InventoryItemSpecificationValue, x => x.MapFrom(dest => dest.InventoryItemSpecificationValue));

            #endregion

            #region Distribution

            CreateMap<Distribution, DistributionDTO>()
                .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
                .ForMember(dest => dest.DistributionId, d => d.MapFrom(src => src.DistributionId))
                .ForMember(dest => dest.DistributionDate, d => d.MapFrom(src => src.DistributionDate))
                .ForMember(dest => dest.ResponsibleEmployeeId, d => d.MapFrom(src => src.ResponsibleEmployeeId))
                .ForMember(dest => dest.ResponsibleStockEmployeeId, d => d.MapFrom(src => src.ResponsibleStockEmployeeId))
                .ForMember(dest => dest.InventoryItemTransactionId, d => d.MapFrom(src => src.InventoryItemTransactionId))
                .ForMember(dest => dest.InventoryItemTransaction, d => d.MapFrom(src => src.InventoryItemTransaction))
                .ReverseMap()
                .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
                .ForMember(dest => dest.ResponsibleEmployeeId, d => d.MapFrom(src => src.ResponsibleEmployeeId))
                .ForMember(dest => dest.ResponsibleStockEmployeeId, d => d.MapFrom(src => src.ResponsibleStockEmployeeId))
                .ForMember(dest => dest.DistributionDate, d => d.MapFrom(src => src.DistributionDate))
                .ForMember(dest => dest.DistributionId, d => d.MapFrom(src => src.DistributionId))
                .ForMember(dest => dest.InventoryItemTransactionId, d => d.MapFrom(src => src.InventoryItemTransactionId))
                .ForMember(dest => dest.InventoryItemTransaction, d => d.MapFrom(src => src.InventoryItemTransaction));

            #endregion

            #region DistributionRecord

            CreateMap<DistributionRecord, DistributionRecordDTO>()
                .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
                .ForMember(dest => dest.DistributionId, d => d.MapFrom(src => src.DistributionId))
                .ForMember(dest => dest.InventoryItemId, d => d.MapFrom(src => src.InventoryItemId))
                .ForMember(dest => dest.WorkplaceId, d => d.MapFrom(src => src.WorkplaceId))
                .ForMember(dest => dest.Quantity, d => d.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.Distribution, d => d.MapFrom(src => src.Distribution))
                .ForMember(dest => dest.InventoryItem, d => d.MapFrom(src => src.InventoryItem))
                .ForMember(dest => dest.Workplace, d => d.MapFrom(src => src.Workplace))
                .ReverseMap()
                .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
                .ForMember(dest => dest.InventoryItemId, d => d.MapFrom(src => src.InventoryItemId))
                .ForMember(dest => dest.DistributionId, d => d.MapFrom(src => src.DistributionId))
                .ForMember(dest => dest.WorkplaceId, d => d.MapFrom(src => src.WorkplaceId))
                .ForMember(dest => dest.Quantity, d => d.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.InventoryItem, d => d.MapFrom(src => src.InventoryItem))
                .ForMember(dest => dest.Distribution, d => d.MapFrom(src => src.Distribution))
                .ForMember(dest => dest.Workplace, d => d.MapFrom(src => src.Workplace));

            #endregion

            //#region Employee

            //CreateMap<Employee, EmployeeDTO>()
            //    .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
            //    .ForMember(dest => dest.LastName, x => x.MapFrom(dest => dest.LastName))
            //    .ForMember(dest => dest.FirstName, x => x.MapFrom(dest => dest.FirstName))
            //    .ForMember(dest => dest.MiddleName, x => x.MapFrom(dest => dest.MiddleName))
            //    .ForMember(dest => dest.Phone, x => x.MapFrom(dest => dest.Phone))
            //    .ReverseMap()
            //    .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
            //    .ForMember(dest => dest.LastName, x => x.MapFrom(dest => dest.LastName))
            //    .ForMember(dest => dest.FirstName, x => x.MapFrom(dest => dest.FirstName))
            //    .ForMember(dest => dest.MiddleName, x => x.MapFrom(dest => dest.MiddleName))
            //    .ForMember(dest => dest.Phone, x => x.MapFrom(dest => dest.Phone));


            //#endregion

            //#region Building

            //CreateMap<Building, BuildingDTO>()
            //    .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.Name, d => d.MapFrom(src => src.Name))
            //    .ForMember(dest => dest.Address, d => d.MapFrom(src => src.Address))
            //    .ForMember(dest => dest.NumberOfFloors, d => d.MapFrom(src => src.NumberOfFloors))
            //    .ReverseMap()
            //    .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.Name, d => d.MapFrom(src => src.Name))
            //    .ForMember(dest => dest.Address, d => d.MapFrom(src => src.Address))
            //    .ForMember(dest => dest.NumberOfFloors, d => d.MapFrom(src => src.NumberOfFloors));
            //#endregion

            //#region Department

            //CreateMap<Department, DepartmentDTO>()
            //    .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.Name, d => d.MapFrom(src => src.Name))
            //    .ForMember(dest => dest.Orders, d => d.MapFrom(src => src.Orders))
            //    .ForMember(dest => dest.Subdivision, d => d.MapFrom(src => src.Subdivision))
            //    .ReverseMap()
            //    .ForMember(dest => dest.SubdivisionId, d => d.MapFrom(src => src.Subdivision.Id))
            //    .ForMember(dest => dest.Id, d => d.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.Name, d => d.MapFrom(src => src.Name))
            //    .ForMember(dest => dest.Orders, d => d.MapFrom(src => src.Orders))
            //    .ForMember(dest => dest.Subdivision, d => d.MapFrom(src => src.Subdivision));

            //#endregion

            #region Order

            CreateMap<Order, OrderDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.OrderId, x => x.MapFrom(dest => dest.OrderId))
                .ForMember(dest => dest.Comment, x => x.MapFrom(dest => dest.Comment))
                .ForMember(dest => dest.OrderDate, x => x.MapFrom(dest => dest.OrderDate))
                .ForMember(dest => dest.DepartmentId, x => x.MapFrom(dest => dest.DepartmentId))
                .ForMember(dest => dest.EmployeeId, x => x.MapFrom(dest => dest.EmployeeId))
                .ForMember(dest => dest.InventoryItemTransaction, x => x.MapFrom(dest => dest.InventoryItemTransaction))
                .ForMember(dest => dest.InventoryItemTransactionId, x => x.MapFrom(dest => dest.InventoryItemTransaction.Id))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.OrderId, x => x.MapFrom(dest => dest.OrderId))
                .ForMember(dest => dest.Comment, x => x.MapFrom(dest => dest.Comment))
                .ForMember(dest => dest.OrderDate, x => x.MapFrom(dest => dest.OrderDate))
                .ForMember(dest => dest.DepartmentId, x => x.MapFrom(dest => dest.DepartmentId))
                .ForMember(dest => dest.EmployeeId, x => x.MapFrom(dest => dest.EmployeeId))
                .ForMember(dest => dest.InventoryItemTransaction, x => x.MapFrom(dest => dest.InventoryItemTransaction));


            #endregion

            #region OrderRecord

            CreateMap<OrderRecord, OrderRecordDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.OrderRecordId, x => x.MapFrom(dest => dest.OrderRecordId))
                .ForMember(dest => dest.Order, x => x.MapFrom(dest => dest.Order))
                .ForMember(dest => dest.OrderId, x => x.MapFrom(dest => dest.OrderId))
                .ForMember(dest => dest.InventoryItem, x => x.MapFrom(dest => dest.InventoryItem))
                .ForMember(dest => dest.InventoryItemId, x => x.MapFrom(dest => dest.InventoryItem.InventoryItemId))
                .ForMember(dest => dest.SerialNumber, x => x.MapFrom(dest => dest.SerialNumber))
                .ForMember(dest => dest.Product, x => x.MapFrom(dest => dest.Product))
                .ForMember(dest => dest.ProductId, x => x.MapFrom(dest => dest.ProductId))
                .ForMember(dest => dest.Quantity, x => x.MapFrom(dest => dest.Quantity))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.OrderRecordId, x => x.MapFrom(dest => dest.OrderRecordId))
                .ForMember(dest => dest.Order, x => x.MapFrom(dest => dest.Order))
                .ForMember(dest => dest.InventoryItem, x => x.MapFrom(dest => dest.InventoryItem))
                .ForMember(dest => dest.Product, x => x.MapFrom(dest => dest.Product))
                .ForMember(dest => dest.Quantity, x => x.MapFrom(dest => dest.Quantity))
                .ForMember(dest => dest.SerialNumber, x => x.MapFrom(dest => dest.SerialNumber))
                .ForMember(dest => dest.OrderId, x => x.MapFrom(dest => dest.OrderId))
                .ForMember(dest => dest.ProductId, x => x.MapFrom(dest => dest.ProductId));

            #endregion

            #region WorkPlace

            CreateMap<Workplace, WorkPlaceDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.WorkplaceId, x => x.MapFrom(dest => dest.WorkplaceId))
                .ForMember(dest => dest.BuildingId, x => x.MapFrom(dest => dest.BuildingId))
                .ForMember(dest => dest.DepartmentId, x => x.MapFrom(dest => dest.DepartmentId))
                .ForMember(dest => dest.RoomId, x => x.MapFrom(dest => dest.RoomId))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.BuildingId, x => x.MapFrom(dest => dest.BuildingId))
                .ForMember(dest => dest.WorkplaceId, x => x.MapFrom(dest => dest.WorkplaceId))
                .ForMember(dest => dest.DepartmentId, x => x.MapFrom(dest => dest.DepartmentId))
                .ForMember(dest => dest.RoomId, x => x.MapFrom(dest => dest.RoomId));

            #endregion

            #region Product

            CreateMap<Product, ProductDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.ProductId, x => x.MapFrom(dest => dest.ProductId))
                .ForMember(dest => dest.CategoryName, x => x.MapFrom(dest => dest.ProductCategory.CategoryName))
                .ForMember(dest => dest.ProductCategory, x => x.MapFrom(dest => dest.ProductCategory))
                .ForMember(dest => dest.FullName, x => x.MapFrom(dest => dest.FullName))
                .ForMember(dest => dest.ShortName, x => x.MapFrom(dest => dest.ShortName))
                .ForMember(dest => dest.Vendor, x => x.MapFrom(dest => dest.Vendor))
                .ForMember(dest => dest.ProductSpecificationBlank, x => x.MapFrom(dest => dest.ProductSpecificationBlank))
                .ForMember(dest => dest.ProductSpecificationBlankId, x => x.MapFrom(dest => dest.ProductSpecificationBlank.ProductSpecificationBlankId))
                .ForMember(dest => dest.ProductCategoryId, x => x.MapFrom(dest => dest.ProductCategory.Id))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.ProductId, x => x.MapFrom(dest => dest.ProductId))
                .ForMember(dest => dest.FullName, x => x.MapFrom(dest => dest.FullName))
                .ForMember(dest => dest.ShortName, x => x.MapFrom(dest => dest.ShortName))
                .ForMember(dest => dest.Vendor, x => x.MapFrom(dest => dest.Vendor))
                .ForMember(dest => dest.CategoryName, x => x.MapFrom(dest => dest.CategoryName))
                .ForMember(dest => dest.ProductCategory, x => x.MapFrom(dest => dest.ProductCategory))
                .ForMember(dest => dest.ProductSpecificationBlank, x => x.MapFrom(dest => dest.ProductSpecificationBlank));

            #endregion

            #region ProductCategory

            CreateMap<ProductCategory, ProductCategoryDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.CategoryName, x => x.MapFrom(dest => dest.CategoryName))
                .ForMember(dest => dest.Description, x => x.MapFrom(dest => dest.Description))
                .ForMember(dest => dest.IsBatch, x => x.MapFrom(dest => dest.IsBatch))
                .ForMember(dest => dest.IsSpecRequired, x => x.MapFrom(dest => dest.IsSpecRequired))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.CategoryName, x => x.MapFrom(dest => dest.CategoryName))
                .ForMember(dest => dest.Description, x => x.MapFrom(dest => dest.Description))
                .ForMember(dest => dest.IsBatch, x => x.MapFrom(dest => dest.IsBatch))
                .ForMember(dest => dest.IsSpecRequired, x => x.MapFrom(dest => dest.IsSpecRequired));

            #endregion

            #region Specification

            CreateMap<Specification, SpecificationDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.IsRequered, x => x.MapFrom(dest => dest.IsRequered))
                .ForMember(dest => dest.SpecificationId, x => x.MapFrom(dest => dest.SpecificationId))
                .ForMember(dest => dest.SpecificationBlank, x => x.MapFrom(dest => dest.SpecificationBlank))
                .ForMember(dest => dest.SpecificationBlankId, x => x.MapFrom(dest => dest.SpecificationBlankId))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.IsRequered, x => x.MapFrom(dest => dest.IsRequered))
                .ForMember(dest => dest.SpecificationBlank, x => x.MapFrom(dest => dest.SpecificationBlank))
                .ForMember(dest => dest.SpecificationBlankId, x => x.MapFrom(dest => dest.SpecificationBlank.SpecificationBlankId))
                .ForMember(dest => dest.SpecificationId, x => x.MapFrom(dest => dest.SpecificationId));
            CreateMap<SpecificationDTO, SpecificationViewModel>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.IsRequered, x => x.MapFrom(dest => dest.IsRequered))
                .ForMember(dest => dest.SpecificationId, x => x.MapFrom(dest => dest.SpecificationId))
                .ForMember(dest => dest.SpecificationBlankId, x => x.MapFrom(dest => dest.SpecificationBlank.SpecificationBlankId))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.IsRequered, x => x.MapFrom(dest => dest.IsRequered))
                .ForMember(dest => dest.SpecificationBlankId, x => x.MapFrom(dest => dest.SpecificationBlankId))
                .ForMember(dest => dest.SpecificationId, x => x.MapFrom(dest => dest.SpecificationId))
                ;

            #endregion

            #region SpecificationBlank

            CreateMap<SpecificationBlank, SpecificationBlankDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.SpecificationBlankId, x => x.MapFrom(dest => dest.SpecificationBlankId))
                .ForMember(dest => dest.Specifications, x => x.MapFrom(dest => dest.Specifications))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.SpecificationBlankId, x => x.MapFrom(dest => dest.SpecificationBlankId))
                .ForMember(dest => dest.Specifications, x => x.MapFrom(dest => dest.Specifications))
                ;

            CreateMap<SpecificationBlankDTO, SpecificationBlankViewModel>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.SpecificationBlankId, x => x.MapFrom(dest => dest.SpecificationBlankId))
                .ForMember(dest => dest.Specifications, x => x.MapFrom(dest => dest.Specifications))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.SpecificationBlankId, x => x.MapFrom(dest => dest.SpecificationBlankId))
                .ForMember(dest => dest.Specifications, x => x.MapFrom(dest => dest.Specifications))
                ;

            #endregion

            #region ProductSpecificationBlank

            CreateMap<ProductSpecificationBlank, ProductSpecificationBlankDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.ProductSpecificationBlankId, x => x.MapFrom(dest => dest.ProductSpecificationBlankId))
                .ForMember(dest => dest.ProductId, x => x.MapFrom(dest => dest.ProductId))
                .ForMember(dest => dest.Product, x => x.MapFrom(dest => dest.Product))
                .ForMember(dest => dest.SpecificationBlank, x => x.MapFrom(dest => dest.SpecificationBlank))
                .ForMember(dest => dest.SpecificationBlankId, x => x.MapFrom(dest => dest.SpecificationBlankId))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.ProductSpecificationBlankId, x => x.MapFrom(dest => dest.ProductSpecificationBlankId))
                .ForMember(dest => dest.Product, x => x.MapFrom(dest => dest.Product))
                .ForMember(dest => dest.ProductId, x => x.MapFrom(dest => dest.Product.ProductId))
                .ForMember(dest => dest.SpecificationBlank, x => x.MapFrom(dest => dest.SpecificationBlank))
                .ForMember(dest => dest.SpecificationBlankId, x => x.MapFrom(dest => dest.SpecificationBlankId));

            #endregion

            #region ProductSpecification

            CreateMap<ProductSpecification, ProductSpecificationDTO>()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.ProductSpecificationId, x => x.MapFrom(dest => dest.ProductSpecificationId))
                .ForMember(dest => dest.ProductSpecificationBlankId, x => x.MapFrom(dest => dest.ProductSpecificationBlankId))
                .ForMember(dest => dest.ProductSpecificationValue, x => x.MapFrom(dest => dest.ProductSpecificationValue))
                .ForMember(dest => dest.ProductSpecificationBlank, x => x.MapFrom(dest => dest.ProductSpecificationBlank))
                .ReverseMap()
                .ForMember(dest => dest.Id, x => x.MapFrom(dest => dest.Id))
                .ForMember(dest => dest.ProductSpecificationBlank, x => x.MapFrom(dest => dest.ProductSpecificationBlank))
                .ForMember(dest => dest.ProductSpecificationBlankId, x => x.MapFrom(dest => dest.ProductSpecificationBlankId))
                .ForMember(dest => dest.ProductSpecificationId, x => x.MapFrom(dest => dest.ProductSpecificationId))
                .ForMember(dest => dest.ProductSpecificationValue, x => x.MapFrom(dest => dest.ProductSpecificationValue));

            #endregion
        }
    }
}
