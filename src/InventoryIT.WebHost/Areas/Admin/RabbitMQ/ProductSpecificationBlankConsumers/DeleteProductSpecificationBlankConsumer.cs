﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductSpecificationBlankModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.ProductSpecificationBlankRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductSpecificationBlankConsumers
{
    public class DeleteProductSpecificationBlankConsumer
        : DeleteEntityConsumer<ProductSpecificationBlankDTO, DeleteProductSpecificationBlankRequest>
    {
        public DeleteProductSpecificationBlankConsumer(IProductSpecificationBlankService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
