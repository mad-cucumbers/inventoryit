﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductSpecificationBlankModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.ProductSpecificationBlankResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductSpecificationBlankConsumers
{
    public class GetAllProductSpecificationBlankConsumer
                : GetAllEntityConsumer<ProductSpecificationBlankDTO,
            ProductSpecificationBlankViewModel,
            GetAllProductSpecificationBlankResponse>
    {
        public GetAllProductSpecificationBlankConsumer(IProductSpecificationBlankService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
