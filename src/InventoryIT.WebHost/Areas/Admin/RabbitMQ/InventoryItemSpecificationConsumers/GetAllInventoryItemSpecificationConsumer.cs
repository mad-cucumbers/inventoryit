﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.InventoryItemSpecificationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.InventoryItemSpecificationResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.InventoryItemSpecificationConsumers
{
    public class GetAllInventoryItemSpecificationConsumer
                : GetAllEntityConsumer<InventoryItemSpecificationDTO,
            InventoryItemSpecificationViewModel,
            GetAllInventoryItemSpecificationResponse>
    {
        public GetAllInventoryItemSpecificationConsumer(IInventoryItemSpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
