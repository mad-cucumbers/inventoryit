﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.InventoryItemSpecificationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.InventoryItemSpecificationRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.InventoryItemSpecificationConsumers
{
    public class DeleteInventoryItemSpecificationConsumer
        : DeleteEntityConsumer<InventoryItemSpecificationDTO, DeleteInventoryItemSpecificationRequest>
    {
        public DeleteInventoryItemSpecificationConsumer(IInventoryItemSpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
