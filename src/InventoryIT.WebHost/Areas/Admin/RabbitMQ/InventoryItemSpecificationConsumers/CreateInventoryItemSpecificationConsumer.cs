﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.InventoryItemSpecificationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.InventoryItemSpecificationRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.InventoryItemSpecificationConsumers
{
    public class CreateInventoryItemSpecificationConsumer
                : CreateEntityConsumer<InventoryItemSpecificationDTO,
            CreateInventoryItemSpecificationRequest>
    {
        public CreateInventoryItemSpecificationConsumer(IInventoryItemSpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
