﻿using AutoMapper;
using Contracts.ResponseModels;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain;
using InventoryIT.WebHost.Contracts.Requests.BaseRequests;
using InventoryIT.WebHost.Contracts.Responses.BaseResponses;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers
{
    public class GetByIdEntityConsumer<TDTO,TVM, TRequest, TResponse>
        : IConsumer<TRequest>
        where TDTO : BaseEntity
        where TRequest : GetByIdEntityRequest
        where TResponse : GetByIdEntityResponse<TVM>, new()

    {
        private readonly IBaseService<TDTO> _serivece;

        private readonly IMapper _mapper;

        public GetByIdEntityConsumer(IBaseService<TDTO> service, IMapper mapper)
        {
            _serivece = service ?? throw new ArgumentNullException(nameof(service));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Consume(ConsumeContext<TRequest> context)
        {
            var result = await _serivece.GetByIdAsync(context.Message.Id);

            var response = new TResponse();
            if (!result.Success)
                response.Notification = new NotificationViewModel(result.Errors, e: result.AspNetException);
            else
            {
                response.Notification = new NotificationViewModel();
                response.Model = _mapper.Map<TVM>(result.Data);
            }

            await context.RespondAsync(response);
        }
    }
}
