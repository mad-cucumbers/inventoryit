﻿using AutoMapper;
using Contracts.ResponseModels;
using GreenPipes;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain;
using InventoryIT.WebHost.Contracts.Requests.BaseRequests;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers
{
    public class UpdateEntityConsumer
        <TDTO, TRequest>
        : IConsumer<TRequest>
        where TDTO : BaseEntity
        where TRequest : UpdateEntityRequest
    {
        private readonly IBaseService<TDTO> _serivece;

        private readonly IMapper _mapper;

        public UpdateEntityConsumer(IBaseService<TDTO> service, IMapper mapper)
        {
            _serivece = service ?? throw new ArgumentNullException(nameof(service));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Consume(ConsumeContext<TRequest> context)
        {
            var model = _mapper.Map<TDTO>(context.Message);
            var result = await _serivece.UpdateAsync(model);

            var response = result.Success
                ? new NotificationViewModel()
                : new NotificationViewModel(result.Errors, e: result.AspNetException);
            await context.RespondAsync(response);
        }
    }

    public class UpdateEntityConsumerDefinition
        <TDTO, TRequest>
        : ConsumerDefinition<UpdateEntityConsumer<TDTO, TRequest>>
        where TDTO : BaseEntity
        where TRequest : UpdateEntityRequest
    {
        public UpdateEntityConsumerDefinition()
        {
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<UpdateEntityConsumer<TDTO, TRequest>> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }
    }
}
