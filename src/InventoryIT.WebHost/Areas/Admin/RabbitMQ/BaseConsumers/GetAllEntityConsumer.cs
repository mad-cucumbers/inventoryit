﻿using AutoMapper;
using Contracts.ResponseModels;
using GreenPipes;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain;
using InventoryIT.WebHost.Contracts.Requests.BaseRequests;
using InventoryIT.WebHost.Contracts.Responses.BaseResponses;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers
{
    public class GetAllEntityConsumer<TDTO, TRequest, TResponse>
        : IConsumer<TRequest>
        where TDTO : BaseEntity
        where TRequest : BaseEntity
        where TResponse : GetAllEntityResponse<TRequest>, new()
    {
        private readonly IBaseService<TDTO> _serivece;

        private readonly IMapper _mapper;

        public GetAllEntityConsumer(IBaseService<TDTO> service, IMapper mapper)
        {
            _serivece = service ?? throw new ArgumentNullException(nameof(service));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Consume(ConsumeContext<TRequest> context)
        {
            var result = await _serivece.GetAllAsync();

            var response = new TResponse();
            if (!result.Success)
                response.Notification = new NotificationViewModel(result.Errors, e: result.AspNetException);
            else
            {
                response.Notification = new NotificationViewModel();
                response.Model = _mapper.Map<IEnumerable<TRequest>>(result.Data);
            }

            await context.RespondAsync(response);
        }
    }

    //public class AllSpecofocationConsumerDefinition
    //    <TDTO, TRequest, TVM>
    //    : ConsumerDefinition<GetAllConsumer<TDTO, TRequest, TVM>>
    //    where TDTO : BaseEntity
    //    where TRequest : GetAllRequest<TVM>
    //{
    //    public AllSpecofocationConsumerDefinition()
    //    {
    //    }

    //    protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<GetAllConsumer<TDTO, TRequest, TVM>> consumerConfigurator)
    //    {
    //        endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
    //    }

    //}
}
