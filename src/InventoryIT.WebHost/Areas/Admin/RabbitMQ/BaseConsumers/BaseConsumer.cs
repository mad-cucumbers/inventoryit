﻿using AutoMapper;
using Contracts.ResponseModels;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain;
using InventoryIT.WebHost.Contracts.Requests.BaseRequests;
using InventoryIT.WebHost.Contracts.Responses.BaseResponses;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers
{
    public class BaseConsumer
        <TDTO, TCreateEntityRequest, TDeleteEntityRequest, TUpdateEntityRequest, TGetAllEntityRequest, TResponse, VM>
        : IConsumer<TCreateEntityRequest>
        where TDTO : BaseEntity
        where TCreateEntityRequest : CreateEntityRequest
        where TDeleteEntityRequest : DeleteEntityRequest
        where TUpdateEntityRequest : UpdateEntityRequest
        where TGetAllEntityRequest : GetAllEntityRequest
        where TResponse : GetAllEntityResponse<VM>, new()
    {
        private readonly IBaseService<TDTO> _serivece;

        private readonly IMapper _mapper;

        public BaseConsumer(IBaseService<TDTO> service, IMapper mapper)
        {
            _serivece = service ?? throw new ArgumentNullException(nameof(service));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Consume(ConsumeContext<TCreateEntityRequest> context)
        {
            var model = _mapper.Map<TDTO>(context.Message);
            var result = await _serivece.AddAsync(model);

            var response = result.Success
                ? new NotificationViewModel()
                : new NotificationViewModel(result.Errors, e: result.AspNetException);
            await context.RespondAsync(response);
        }
        
        public async Task Consume(ConsumeContext<TGetAllEntityRequest> context)
        {
            var result = await _serivece.GetAllAsync();

            var response = new TResponse();
            if (!result.Success)
                response.Notification = new NotificationViewModel(result.Errors, e: result.AspNetException);
            else
            {
                response.Notification = new NotificationViewModel();
                response.Model = _mapper.Map<IEnumerable<VM>>(result.Data);
            }

            await context.RespondAsync(response);
        }

        public async Task Consume(ConsumeContext<TDeleteEntityRequest> context)
        {
            var model = _mapper.Map<TDTO>(context.Message);
            var result = await _serivece.DeleteAsync(model.Id);

            var response = result.Success
                ? new NotificationViewModel()
                : new NotificationViewModel(result.Errors, e: result.AspNetException);
            await context.RespondAsync(response);
        }
    }
}
