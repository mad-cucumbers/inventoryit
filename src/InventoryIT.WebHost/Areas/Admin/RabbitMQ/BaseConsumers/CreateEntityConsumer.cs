﻿using AutoMapper;
using Contracts.ResponseModels;
using GreenPipes;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain;
using InventoryIT.WebHost.Contracts.Requests.BaseRequests;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers
{
    public class CreateEntityConsumer
        <TDTO, TRequest>
        : IConsumer<TRequest>
        where TDTO : BaseEntity
        where TRequest : CreateEntityRequest
    {
        private readonly IBaseService<TDTO> _serivece;

        private readonly IMapper _mapper;

        public CreateEntityConsumer(IBaseService<TDTO> service, IMapper mapper)
        {
            _serivece = service ?? throw new ArgumentNullException(nameof(service));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task Consume(ConsumeContext<TRequest> context)
        {
            var model = _mapper.Map<TDTO>(context.Message);
            var result = await _serivece.AddAsync(model);

            var response = result.Success
                ? new NotificationViewModel()
                : new NotificationViewModel(result.Errors, e: result.AspNetException);
            await context.RespondAsync(response);
        }
    }

    public class CreateSpecificationConsumerDefinition
        <TDTO, TRequest>
        : ConsumerDefinition<CreateEntityConsumer<TDTO, TRequest>>
        where TDTO : BaseEntity
        where TRequest : CreateEntityRequest
    {
        public CreateSpecificationConsumerDefinition()
        {
            EndpointName = "CreateSpecification";
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<CreateEntityConsumer<TDTO, TRequest>> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }
    }
}