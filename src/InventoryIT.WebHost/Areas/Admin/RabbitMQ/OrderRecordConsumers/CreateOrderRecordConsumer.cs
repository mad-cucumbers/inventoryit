﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.OrderRecordModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.OrderRecordRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.OrderRecordConsumers
{
    public class CreateOrderRecordConsumer
                : CreateEntityConsumer<OrderRecordDTO,
            CreateOrderRecordRequest>
    {
        public CreateOrderRecordConsumer(IOrderRecordService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
