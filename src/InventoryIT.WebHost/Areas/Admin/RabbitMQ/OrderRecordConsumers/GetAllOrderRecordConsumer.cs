﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.OrderRecordModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.OrderRecordResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.OrderRecordConsumers
{
    public class GetAllOrderRecordConsumer
                : GetAllEntityConsumer<OrderRecordDTO,
            OrderRecordViewModel,
            GetAllOrderRecordResponse>
    {
        public GetAllOrderRecordConsumer(IOrderRecordService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
