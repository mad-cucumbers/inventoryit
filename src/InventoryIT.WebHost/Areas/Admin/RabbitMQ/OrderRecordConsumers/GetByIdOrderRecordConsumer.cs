﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.OrderRecordModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.OrderRecordRequests;
using InventoryIT.WebHost.Contracts.Responses.OrderRecordResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.OrderRecordConsumers
{
    public class GetByIdOrderRecordConsumer
                : GetByIdEntityConsumer<OrderRecordDTO,
            OrderRecordViewModel,
            GetByIdOrderRecordRequest,
            GetByIdOrderRecordResponse>
    {
        public GetByIdOrderRecordConsumer(IOrderRecordService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
