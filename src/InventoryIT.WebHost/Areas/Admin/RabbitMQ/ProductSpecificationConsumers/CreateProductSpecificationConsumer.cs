﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductSpecificationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.ProductSpecificationRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductSpecificationConsumers
{
    public class CreateProductSpecificationConsumer
                : CreateEntityConsumer<ProductSpecificationDTO,
            CreateProductSpecificationRequest>
    {
        public CreateProductSpecificationConsumer(IProductSpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
