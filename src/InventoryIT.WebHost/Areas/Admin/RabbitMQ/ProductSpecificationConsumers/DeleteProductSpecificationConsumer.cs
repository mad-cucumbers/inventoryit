﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductSpecificationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.ProductSpecificationRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductSpecificationConsumers
{
    public class DeleteProductSpecificationConsumer
        : DeleteEntityConsumer<ProductSpecificationDTO, DeleteProductSpecificationRequest>
    {
        public DeleteProductSpecificationConsumer(IProductSpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
