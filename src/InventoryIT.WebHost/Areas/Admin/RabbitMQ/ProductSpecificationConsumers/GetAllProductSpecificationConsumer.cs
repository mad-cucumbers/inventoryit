﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductSpecificationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.ProductSpecificationResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductSpecificationConsumers
{
    public class GetAllProductSpecificationConsumer
                : GetAllEntityConsumer<ProductSpecificationDTO,
            ProductSpecificationViewModel,
            GetAllProductSpecificationResponse>
    {
        public GetAllProductSpecificationConsumer(IProductSpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
