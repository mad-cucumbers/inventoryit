﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductCategoryModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.ProductCategoryRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductCategoryConsumers
{
    public class DeleteProductCategoryConsumer
        : DeleteEntityConsumer<ProductCategoryDTO, DeleteProductCategoryRequest>
    {
        public DeleteProductCategoryConsumer(IProductCategoryService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
