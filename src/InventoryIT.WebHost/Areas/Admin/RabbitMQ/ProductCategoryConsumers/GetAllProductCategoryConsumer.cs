﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductCategoryModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.ProductCategoryResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductCategoryConsumers
{
    public class GetAllProductCategoryConsumer
                : GetAllEntityConsumer<ProductCategoryDTO,
            ProductCategoryViewModel,
            GetAllProductCategoryResponse>
    {
        public GetAllProductCategoryConsumer(IProductCategoryService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
