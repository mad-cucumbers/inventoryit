﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.ProductRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductConsumers
{
    public class UpdateProductConsumer
                : UpdateEntityConsumer<ProductDTO,
            UpdateProductRequest>
    {
        public UpdateProductConsumer(IProductService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
