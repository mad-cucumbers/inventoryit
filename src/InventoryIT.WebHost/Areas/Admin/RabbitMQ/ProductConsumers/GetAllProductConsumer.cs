﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.ProductResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductConsumers
{
    public class GetAllProductConsumer
                : GetAllEntityConsumer<ProductDTO,
            ProductViewModel,
            GetAllProductResponse>
    {
        public GetAllProductConsumer(IProductService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
