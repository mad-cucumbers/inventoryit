﻿using AutoMapper;
using InventoryIT.Core.Models.AmortisationReasonModels;
using InventoryIT.DataAccess.Services;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.AmortisationReasonRequests;
using InventoryIT.WebHost.Contracts.Responses.AmortisationReasonResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationReasonConsumers
{
    public class GetByIdAmortisationReasonConsumer
        : GetByIdEntityConsumer<AmortisationReasonDTO, AmortisationReasonViewModel, GetByIdAmortisationReasonRequest, GetByIdAmortisationReasonResponse>
    {
        public GetByIdAmortisationReasonConsumer(AmortisationReasonService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
