﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.AmortisationReasonModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.AmortisationReasonRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationReasonConsumers
{
    public class UpdateAmortisationReasonConsumer
                : UpdateEntityConsumer<AmortisationReasonDTO,
            UpdateAmortisationReasonRequest>
    {
        public UpdateAmortisationReasonConsumer(IAmortisationReasonService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
