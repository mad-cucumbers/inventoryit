﻿using AutoMapper;
using InventoryIT.Core.Models.AmortisationReasonModels;
using InventoryIT.DataAccess.Services;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.AmortisationReasonRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationReasonConsumers
{
    public class DeleteAmortisationReasonConsumer
        : DeleteEntityConsumer<AmortisationReasonDTO, DeleteAmortisationReasonRequest>
    {
        public DeleteAmortisationReasonConsumer(AmortisationReasonService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
