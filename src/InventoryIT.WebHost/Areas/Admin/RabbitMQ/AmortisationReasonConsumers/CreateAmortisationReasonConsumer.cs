﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.AmortisationReasonModels;
using InventoryIT.DataAccess.Services;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.AmortisationReasonRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationReasonConsumers
{
    public class CreateAmortisationReasonConsumer
        : CreateEntityConsumer<AmortisationReasonDTO, CreateAmortisationReasonRequest>
    {
        public CreateAmortisationReasonConsumer(AmortisationReasonService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
