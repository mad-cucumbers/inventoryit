﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.DistributionModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.DistributionRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.DistributionConsumers
{
    public class DeleteDistributionConsumer
                : DeleteEntityConsumer<DistributionDTO,
            DeleteDistributionRequest>
    {
        public DeleteDistributionConsumer(IDistributionService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
