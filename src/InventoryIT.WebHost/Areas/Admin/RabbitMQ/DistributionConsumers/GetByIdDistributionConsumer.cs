﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.DistributionModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.DistributionRequests;
using InventoryIT.WebHost.Contracts.Responses.DistributionResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.DistributionConsumers
{
    public class GetByIdDistributionConsumer
                : GetByIdEntityConsumer<DistributionDTO,
            DistributionViewModel,
            GetByIdDistributionRequest,
            GetByIdDistributionResponse>
    {
        public GetByIdDistributionConsumer(IDistributionService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
