﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.WorkPlaceModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.WorkPlaceRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.WorkPlaceConsumers
{
    public class UpdateWorkPlaceConsumer
                : UpdateEntityConsumer<WorkPlaceDTO,
            UpdateWorkPlaceRequest>
    {
        public UpdateWorkPlaceConsumer(IWorkPlaceService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
