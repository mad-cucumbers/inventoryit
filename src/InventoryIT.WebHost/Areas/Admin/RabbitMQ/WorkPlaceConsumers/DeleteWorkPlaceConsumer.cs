﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.WorkPlaceModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.WorkPlaceRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.WorkPlaceConsumers
{
    public class DeleteWorkPlaceConsumer
        : DeleteEntityConsumer<WorkPlaceDTO, DeleteWorkPlaceRequest>
    {
        public DeleteWorkPlaceConsumer(IWorkPlaceService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
