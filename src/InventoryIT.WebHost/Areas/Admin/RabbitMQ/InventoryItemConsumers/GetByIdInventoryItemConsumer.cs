﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.InventoryItemModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.InventoryItemRequests;
using InventoryIT.WebHost.Contracts.Responses.InventoryItemResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.InventoryItemConsumers
{
    public class GetByIdInventoryItemConsumer
                : GetByIdEntityConsumer<InventoryItemDTO,
            InventoryItemViewModel,
            GetByIdInventoryItemRequest,
            GetByIdInventoryItemResponse>
    {
        public GetByIdInventoryItemConsumer(IInventoryItemService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
