﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.AmortisationRecordModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.AmortisationRecordRequests;
using InventoryIT.WebHost.Contracts.Responses.AmortisationRecordResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationRecordConsumers
{
    public class GetByIdAmortisationRecordConsumer
                : GetByIdEntityConsumer<AmortisationRecordDTO,
            AmortisationRecordViewModel,
            GetByIdAmortisationRecordRequest,
            GetByIdAmortisationRecordResponse>
    {
        public GetByIdAmortisationRecordConsumer(IAmortisationRecordService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
