﻿using AutoMapper;
using InventoryIT.Core.Models.AmortisationRecordModels;
using InventoryIT.DataAccess.Services;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.AmortisationRecordRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationRecordConsumers
{
    public class CreateAmortisationRecordConsumer
            : CreateEntityConsumer<AmortisationRecordDTO, CreateAmortisationRecordRequest>
    {
        public CreateAmortisationRecordConsumer(AmortisationRecordService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
