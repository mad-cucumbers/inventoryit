﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.AmortisationRecordModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.AmortisationRecordResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationRecordConsumers
{
    public class GetAllAmortisationRecordConsumer
                : GetAllEntityConsumer<AmortisationRecordDTO,
            AmortisationRecordViewModel,
            GetAllAmortisationRecordResponse>
    {
        public GetAllAmortisationRecordConsumer(IAmortisationRecordService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
