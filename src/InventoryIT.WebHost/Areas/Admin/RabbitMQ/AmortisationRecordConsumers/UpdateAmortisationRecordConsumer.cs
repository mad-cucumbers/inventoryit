﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.AmortisationRecordModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.AmortisationRecordRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationRecordConsumers
{
    public class UpdateAmortisationRecordConsumer
                : UpdateEntityConsumer<AmortisationRecordDTO,
            UpdateAmortisationRecordRequest>
    {
        public UpdateAmortisationRecordConsumer(IAmortisationRecordService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
