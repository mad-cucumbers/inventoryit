﻿using AutoMapper;
using GreenPipes;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.SpecificationBlankModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.SpecificationBlankRequests;
using InventoryIT.WebHost.Contracts.Responses.SpecificationBlankResponse;
using InventoryIT.WebHost.Models;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.SpecififationBlankConsumers
{
    public class GetAllSpecificationBlankConsumer
        : GetAllEntityConsumer<SpecificationBlankDTO,
            SpecificationBlankViewModel,
            GetAllSpecificationBlankResponse>
    {
        public GetAllSpecificationBlankConsumer(ISpecificationBlankService service, IMapper mapper) : base(service, mapper)
        {
        }
    }

    public class GetAllSpecificationBlankDefinition : ConsumerDefinition<GetAllSpecificationBlankConsumer>
    {
        public GetAllSpecificationBlankDefinition()
        {
            EndpointName = "GetAllSpecificationBlank";
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<GetAllSpecificationBlankConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }

    }

}
