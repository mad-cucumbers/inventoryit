﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.SpecificationBlankModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.SpecificationBlankRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.SpecififationBlankConsumers
{
    public class UpdateSpecificationBlankConsumer
        : UpdateEntityConsumer<SpecificationBlankDTO,
            UpdateSpecificationBlankRequest>
    {
        public UpdateSpecificationBlankConsumer(ISpecificationBlankService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
