﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.AmortisationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.AmortisationRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationConsumers
{
    public class UpdateAmortisationConsumer
                : UpdateEntityConsumer<AmortisationDTO,
            UpdateAmortisationRequest>
    {
        public UpdateAmortisationConsumer(IAmortisationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
