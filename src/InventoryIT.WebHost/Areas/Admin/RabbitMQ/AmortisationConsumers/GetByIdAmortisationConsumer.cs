﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.AmortisationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.AmortisationRequests;
using InventoryIT.WebHost.Contracts.Responses.AmortisationResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationConsumers
{
    public class GetByIdAmortisationConsumer
                : GetByIdEntityConsumer<AmortisationDTO,
            AmortisationViewModel,
            GetByIdAmortisationRequest,
            GetByIdAmortisationResponse>
    {
        public GetByIdAmortisationConsumer(IAmortisationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
