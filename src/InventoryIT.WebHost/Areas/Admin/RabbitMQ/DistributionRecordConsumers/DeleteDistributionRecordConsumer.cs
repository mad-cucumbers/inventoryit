﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.DistributionRecordModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.DistributionRecordRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.DistributionRecordConsumers
{
    public class DeleteDistributionRecordConsumer
                : DeleteEntityConsumer<DistributionRecordDTO,
            DeleteDistributionRecordRequest>
    {
        public DeleteDistributionRecordConsumer(IDistributionRecordService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
