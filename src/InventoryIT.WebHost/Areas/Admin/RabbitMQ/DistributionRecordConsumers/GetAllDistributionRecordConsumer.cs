﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.DistributionRecordModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.DistributionRecordResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.DistributionRecordConsumers
{
    public class GetAllDistributionRecordConsumer
                : GetAllEntityConsumer<DistributionRecordDTO,
            DistributionRecordViewModel,
            GetAllDistributionRecordResponse>
    {
        public GetAllDistributionRecordConsumer(IDistributionRecordService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
