﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.OrderModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.OrderResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.OrderConsumers
{
    public class GetAllOrderConsumer
                : GetAllEntityConsumer<OrderDTO,
            OrderViewModel,
            GetAllOrderResponse>
    {
        public GetAllOrderConsumer(IOrderService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
