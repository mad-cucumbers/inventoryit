﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.OrderModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.OrderRequests;
using InventoryIT.WebHost.Contracts.Responses.OrderResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.OrderConsumers
{
    public class GetByIdOrderConsumer
                : GetByIdEntityConsumer<OrderDTO,
            OrderViewModel,
            GetByIdOrderRequest,
            GetByIdOrderResponse>
    {
        public GetByIdOrderConsumer(IOrderService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
