﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.OrderModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.OrderRequests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.OrderConsumers
{
    public class CreateOrderConsumer
                : CreateEntityConsumer<OrderDTO,
            CreateOrderRequest>
    {
        public CreateOrderConsumer(IOrderService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
