﻿using AutoMapper;
using Contracts.Constants;
using Contracts.ResponseModels;
using GreenPipes;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Domain.Entities;
using InventoryIT.Core.Models.SpecificationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.SpecificationResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.SpecificationConsumer
{
    public class GetAllSpecificationConsumer
                : GetAllEntityConsumer<SpecificationDTO,
            SpecificationViewModel,
            GetAllSpecificationResponse>
    {
        public GetAllSpecificationConsumer(ISpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }

    public class AllSpecofocationConsumerDefinition : ConsumerDefinition<GetAllSpecificationConsumer>
    {
        public AllSpecofocationConsumerDefinition()
        {
            EndpointName = "GetAllSpecification";
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<GetAllSpecificationConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }

    }
}
