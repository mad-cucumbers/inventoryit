﻿using AutoMapper;
using Contracts.ResponseModels;
using GreenPipes;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.SpecificationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.SpecificationRequests;
using InventoryIT.WebHost.Contracts.SpecificationRequests;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.SpecificationConsumer
{
    public class CreateSpecificationConsumer
        : CreateEntityConsumer<SpecificationDTO,
            CreateSpecificationRequest>
    {
        public CreateSpecificationConsumer(ISpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }

    public class CreateSpecificationConsumerDefinition : ConsumerDefinition<CreateSpecificationConsumer>
    {
        public CreateSpecificationConsumerDefinition()
        {
            EndpointName = "CreateSpecification";
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<CreateSpecificationConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }
    }
}
