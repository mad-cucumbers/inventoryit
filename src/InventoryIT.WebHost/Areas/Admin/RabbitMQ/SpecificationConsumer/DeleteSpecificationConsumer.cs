﻿using AutoMapper;
using Contracts.ResponseModels;
using GreenPipes;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.SpecificationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.SpecificationRequests;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.SpecificationConsumer
{
    public class DeleteSpecificationConsumer
        : DeleteEntityConsumer<SpecificationDTO, DeleteSpecificationRequest>
    {
        public DeleteSpecificationConsumer(ISpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }

    public class DeleteSpecificationConsumerDefinition : ConsumerDefinition<DeleteSpecificationConsumer>
    {
        public DeleteSpecificationConsumerDefinition()
        {
            EndpointName = "DeleteSpecification";
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<DeleteSpecificationConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }
    }
}
