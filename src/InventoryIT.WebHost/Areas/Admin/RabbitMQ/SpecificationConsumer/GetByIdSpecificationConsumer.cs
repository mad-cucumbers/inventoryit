﻿using AutoMapper;
using Contracts.ResponseModels;
using GreenPipes;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.SpecificationModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Requests.SpecificationRequests;
using InventoryIT.WebHost.Contracts.Responses.SpecificationResponses;
using InventoryIT.WebHost.Models;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.SpecificationConsumer
{
    public class GetByIdSpecificationConsumer
                : GetByIdEntityConsumer<SpecificationDTO,
            SpecificationViewModel,
            GetByIdSpecificationRequest,
            GetByIdSpecificationResponse>
    {
        public GetByIdSpecificationConsumer(ISpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }

    public class SpecificationByIdConsumerDefinition : ConsumerDefinition<GetByIdSpecificationConsumer>
    {
        public SpecificationByIdConsumerDefinition()
        {
            EndpointName = "GetByIdSpecification";
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<GetByIdSpecificationConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 500, 1000));
        }

    }
}
