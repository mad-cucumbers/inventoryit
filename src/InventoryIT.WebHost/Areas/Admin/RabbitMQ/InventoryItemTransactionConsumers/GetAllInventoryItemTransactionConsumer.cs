﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.InventoryItemTransactionModels;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.BaseConsumers;
using InventoryIT.WebHost.Contracts.Responses.InventoryItemTransactionResponses;
using InventoryIT.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.RabbitMQ.InventoryItemTransactionConsumers
{
    public class GetAllInventoryItemTransactionConsumer
                : GetAllEntityConsumer<InventoryItemTransactionDTO,
            InventoryItemTransactionViewModel,
            GetAllInventoryItemTransactionResponse>
    {
        public GetAllInventoryItemTransactionConsumer(IInventoryItemTransactionService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
