﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.AmortisationReasonModels;
using InventoryIT.DataAccess.Services;
using InventoryIT.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.Controllers
{
    public class AmortisationReasonController : BaseController<AmortisationReasonDTO, AmortisationReasonViewModel>
    {
        public AmortisationReasonController(IAmortisationReasonService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
