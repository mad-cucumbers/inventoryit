﻿using AutoMapper;
using Contracts.Enums;
using Contracts.ResponseModels;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Constants;
using InventoryIT.Core.Domain;
using InventoryIT.Core.ResponseModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.Controllers
{
    [Route("[controller]")]
    public class BaseController<DTO, VM> : ControllerBase where DTO : BaseEntity/* where VM : BaseEntity*/
    {
        private readonly IBaseService<DTO> _serivece;

        private readonly IMapper _mapper;

        public BaseController(IBaseService<DTO> service, IMapper mapper)
        {
            _serivece = service ?? throw new ArgumentNullException(nameof(service));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        // GET: SpecificationController
        [HttpGet]
        [Route("list")]
        public async Task<IActionResult> List()
        {
            var result = await _serivece.GetAllAsync();
            if (!result.Success)
            {
                return Ok(new NotificationViewModel(result.Errors));
            }

            var response = _mapper.Map<IEnumerable<VM>>(result.Data);
            return Ok(new NotificationViewModel<IEnumerable<VM>>(response));
        }

        // GET: SpecificationController/Details/5
        [HttpGet]
        [Route("details")]
        public async Task<ActionResult> Details(int id)
        {
            var result = await _serivece.GetByIdAsync(id);
            if (!result.Success)
            {
                return Ok(new NotificationViewModel(result.Errors));
            }

            var response = _mapper.Map<IEnumerable<VM>>(result.Data);
            return Ok(new NotificationViewModel<IEnumerable<VM>>(response));
        }

        // GET: SpecificationController/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: SpecificationController/Create
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult> Create(VM request)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new NotificationViewModel<VM>
                                                                                      (NotificationType.Error,
                                                                                      request,
                                                                                      InventoryITConstants.Failed
                                                                                      ));
            }

            DTO model = _mapper.Map<DTO>(request);
            InventoryITActionResult result = await _serivece.AddAsync(model);
            return !result.Success ? Ok(new NotificationViewModel(result.Errors)) : Ok(new NotificationViewModel(NotificationType.Success));
        }

        // GET: SpecificationController/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        // POST: SpecificationController/Edit/5
        [HttpPut]
        [Route("update")]
        public async Task<ActionResult> Update(int id, VM request)
        {
            if (!ModelState.IsValid) return Ok(new NotificationViewModel<VM>
                                                                                      (NotificationType.Error,
                                                                                      request,
                                                                                      InventoryITConstants.Failed
                                                                                      ));
            //if (_serivece.ExistEntityByName(request.Name)) return Ok(new NotificationViewModel<CreateOrEditApplicationPriorityVIewModel>
            //                                                                         (ManagementItConstants.messageError,
            //                                                                         TypeOfErrors.ExistNameEntity, request));

            var model = _mapper.Map<DTO>(request);

            var result = await _serivece.UpdateAsync(model);

            if (!result.Success)
            {
                if (result.Errors.Any(x => x == TypeOfErrors.AddingEntityError))
                    return Ok(new NotificationViewModel<VM>(NotificationType.Error,
                                                                                      request,
                                                                                      InventoryITConstants.Failed
                                                                                      ));

                return Ok(new NotificationViewModel<VM>(NotificationType.Error,
                                                                                      request,
                                                                                      InventoryITConstants.Failed
                                                                                      ));
            }

            return Ok(new NotificationViewModel(NotificationType.Success));
        }

        // GET: SpecificationController/Delete/5

        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        // POST: SpecificationController/Delete/5
        [HttpDelete]
        [Route("delete")]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await _serivece.DeleteAsync(id);

            if (!result.Success)
            {
                if (result.Errors.Any(x => x == TypeOfErrors.NotFound))
                    return Ok(new NotificationViewModel(NotificationType.Error,
                                                                                      InventoryITConstants.Failed
                                                                                      ));
                if (result.Errors.Any(x => x == TypeOfErrors.DeletionEntityError))
                    return Ok(new NotificationViewModel(NotificationType.Error,
                                                                                      InventoryITConstants.Failed
                                                                                      ));

                return Ok(new NotificationViewModel(NotificationType.Error,
                                                                                      InventoryITConstants.Failed
                                                                                      ));
            }

            return Ok(new NotificationViewModel(NotificationType.Success));
        }
    }
}
