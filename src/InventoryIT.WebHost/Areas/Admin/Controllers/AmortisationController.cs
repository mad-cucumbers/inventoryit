﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryIT.Core.Constants;
using InventoryIT.Core.Enums;
using InventoryIT.Core.ResponseModels;
using Microsoft.AspNetCore.Authorization;
using Contracts.ResponseModels;
using AutoMapper;
using InventoryIT.Core.Models.AmortisationModels;
using InventoryIT.WebHost.Models;
using InventoryIT.Core.Abstractions.Services;

namespace InventoryIT.WebHost.Areas.Admin.Controllers
{
    [Route("[controller]")]
    public class AmortisationController : BaseController<AmortisationDTO, AmortisationViewModel>
    {
        public AmortisationController(IAmortisationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
