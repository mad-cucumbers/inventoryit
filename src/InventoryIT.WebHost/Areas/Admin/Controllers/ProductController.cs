﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductModels;
using InventoryIT.DataAccess.Services;
using InventoryIT.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.Controllers
{
    public class ProductController : BaseController<ProductDTO, ProductViewModel>
    {
        public ProductController(ProductService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
