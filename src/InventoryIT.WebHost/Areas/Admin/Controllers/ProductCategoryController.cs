﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductCategoryModels;
using InventoryIT.DataAccess.Services;
using InventoryIT.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.Controllers
{
    public class ProductCategoryController : BaseController<ProductCategoryDTO, ProductCategoryViewModel>
    {
        public ProductCategoryController(ProductCategoryService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
