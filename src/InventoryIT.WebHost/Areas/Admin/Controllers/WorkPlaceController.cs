﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.WorkPlaceModels;
using InventoryIT.DataAccess.Services;
using InventoryIT.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.Controllers
{
    public class WorkPlaceController : BaseController<WorkPlaceDTO, WorkplaceViewModel>
    {
        public WorkPlaceController(WorkPlaceService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
