﻿using AutoMapper;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Models.ProductSpecificationModels;
using InventoryIT.DataAccess.Services;
using InventoryIT.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.Controllers
{
    public class ProductSpecificationController : BaseController<ProductSpecificationDTO, ProductSpecificationViewModel>
    {
        public ProductSpecificationController(ProductSpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
