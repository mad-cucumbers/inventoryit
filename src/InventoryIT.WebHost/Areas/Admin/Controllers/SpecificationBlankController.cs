﻿using AutoMapper;
using Contracts.Enums;
using Contracts.ResponseModels;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Constants;
using InventoryIT.Core.Models.SpecificationBlankModels;
using InventoryIT.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.Controllers
{
    [Route("[controller]")]
    public class SpecificationBlankController : BaseController<SpecificationBlankDTO, SpecificationBlankViewModel>
    {
        public SpecificationBlankController(ISpecificationBlankService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
