﻿using AutoMapper;
using Contracts.Enums;
using Contracts.ResponseModels;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.Core.Constants;
using InventoryIT.Core.Models.SpecificationModels;
using InventoryIT.WebHost.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Areas.Admin.Controllers
{
    [Route("[controller]")]
    public class SpecificationController : BaseController<SpecificationDTO, SpecificationViewModel>
    {
        //private readonly ISpecificationService _specificationSerivece;

        //private readonly IMapper _mapper;

        public SpecificationController(ISpecificationService service, IMapper mapper) : base(service, mapper)
        {
        }

        //// GET: SpecificationController
        //[HttpGet]
        //[Route("list")]
        //public async Task<IActionResult> List()
        //{
        //    var result = await _specificationSerivece.GetAllAsync();
        //    if (!result.Success)
        //    {
        //        return Ok(new NotificationViewModel(result.Errors));
        //    }

        //    var response = _mapper.Map<IEnumerable<SpecificationViewModel>>(result.Data);
        //    return Ok(new NotificationViewModel<IEnumerable<SpecificationViewModel>>(response));
        //}

        //// GET: SpecificationController/Details/5
        //[HttpGet]
        //[Route("details")]
        //public async Task<ActionResult> Details(int id)
        //{
        //    var result = await _specificationSerivece.GetByIdAsync(id);
        //    if (!result.Success)
        //    {
        //        return Ok(new NotificationViewModel(result.Errors));
        //    }

        //    var response = _mapper.Map<IEnumerable<SpecificationViewModel>>(result.Data);
        //    return Ok(new NotificationViewModel<IEnumerable<SpecificationViewModel>>(response));
        //}

        //// GET: SpecificationController/Create
        ////public ActionResult Create()
        ////{
        ////    return View();
        ////}

        //// POST: SpecificationController/Create
        //[HttpPost]
        //[Route("create")]
        //public async Task<ActionResult> Create(SpecificationViewModel request)
        //{
        //    if (!ModelState.IsValid) return Ok(new NotificationViewModel<SpecificationViewModel>
        //                                                                              (NotificationType.Error,
        //                                                                              request,
        //                                                                              InventoryITConstants.Failed
        //                                                                              ));
        //    var model = _mapper.Map<SpecificationDTO>(request);
        //    var result = await _specificationSerivece.AddAsync(model);
        //    if (!result.Success)
        //    {
        //        return Ok(new NotificationViewModel(result.Errors));
        //    }

        //    return Ok(new NotificationViewModel(NotificationType.Success));
        //}

        //// GET: SpecificationController/Edit/5
        ////public ActionResult Edit(int id)
        ////{
        ////    return View();
        ////}

        //// POST: SpecificationController/Edit/5
        //[HttpPut]
        //[Route("update")]
        //public async Task<ActionResult> Update(int id, SpecificationViewModel request)
        //{
        //    if (!ModelState.IsValid) return Ok(new NotificationViewModel<SpecificationViewModel>
        //                                                                              (Contracts.Enums.NotificationType.Error,
        //                                                                              request,
        //                                                                              InventoryITConstants.Failed
        //                                                                              ));
        //    //if (_specificationSerivece.ExistEntityByName(request.Name)) return Ok(new NotificationViewModel<CreateOrEditApplicationPriorityVIewModel>
        //    //                                                                         (ManagementItConstants.messageError,
        //    //                                                                         TypeOfErrors.ExistNameEntity, request));
        //    var model = new SpecificationDTO()
        //    {
        //        Id = request.Id,
        //        IsRequered = request.IsRequered,
        //        SpecificationBlankId = request.BlankId,
        //        SpecificationId = request.SpecId
        //    };
        //    var result = await _specificationSerivece.UpdateAsync(model);

        //    if (!result.Success)
        //    {
        //        if (result.Errors.Any(x => x == TypeOfErrors.AddingEntityError))
        //            return Ok(new NotificationViewModel<SpecificationViewModel>(Contracts.Enums.NotificationType.Error,
        //                                                                              request,
        //                                                                              InventoryITConstants.Failed
        //                                                                              ));

        //        return Ok(new NotificationViewModel<SpecificationViewModel>(Contracts.Enums.NotificationType.Error,
        //                                                                              request,
        //                                                                              InventoryITConstants.Failed
        //                                                                              ));
        //    }

        //    return Ok(new NotificationViewModel(Contracts.Enums.NotificationType.Success));
        //}

        //// GET: SpecificationController/Delete/5
 
        ////public ActionResult Delete(int id)
        ////{
        ////    return View();
        ////}

        //// POST: SpecificationController/Delete/5
        //[HttpDelete]
        //[Route("delete")]
        //public async Task<ActionResult> Delete(int id, IFormCollection collection)
        //{
        //    var result = await _specificationSerivece.DeleteAsync(id);

        //    if (!result.Success)
        //    {
        //        if (result.Errors.Any(x => x == TypeOfErrors.NotFound))
        //            return Ok(new NotificationViewModel(Contracts.Enums.NotificationType.Error,
        //                                                                              InventoryITConstants.Failed
        //                                                                              ));
        //        if (result.Errors.Any(x => x == TypeOfErrors.DeletionEntityError))
        //            return Ok(new NotificationViewModel(Contracts.Enums.NotificationType.Error,
        //                                                                              InventoryITConstants.Failed
        //                                                                              ));

        //        return Ok(new NotificationViewModel(Contracts.Enums.NotificationType.Error,
        //                                                                              InventoryITConstants.Failed
        //                                                                              ));
        //    }

        //    return Ok(new NotificationViewModel(Contracts.Enums.NotificationType.Success));
        //}
    }
}
