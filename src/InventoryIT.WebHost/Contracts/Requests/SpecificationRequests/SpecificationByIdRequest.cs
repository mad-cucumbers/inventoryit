﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Contracts.SpecificationRequests
{ 
    public class SpecificationByIdRequest
    {
        public SpecificationByIdRequest() { }

        public int Id { get; set; }
    }
}
