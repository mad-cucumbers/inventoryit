﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Contracts.Responses.BaseResponses
{
    public class GetByIdEntityResponse<TVM> : BaseResponse
    {
        public TVM Model { get; set; }
    }
}
