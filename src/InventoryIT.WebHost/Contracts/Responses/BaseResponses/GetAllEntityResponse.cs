﻿using Contracts.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryIT.WebHost.Contracts.Responses.BaseResponses
{
    public class GetAllEntityResponse <TVM> : BaseResponse
    {
        public IEnumerable<TVM> Model { get; set; }
    }
}
