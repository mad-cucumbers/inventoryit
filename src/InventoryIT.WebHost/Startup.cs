using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using InventoryIT.DataAccess;
using InventoryIT.DataAccess.Mapper;
using InventoryIT.Core.Abstractions.Repositories;
using InventoryIT.DataAccess.Repositories;
using Microsoft.OpenApi.Models;
using InventoryIT.Core.Abstractions;
using InventoryIT.Core.Abstractions.Services;
using InventoryIT.DataAccess.Services;
using MassTransit;
using MassTransit.Definition;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.SpecififationBlankConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.SpecificationConsumer;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationReasonConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.AmortisationRecordConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.DistributionRecordConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.DistributionConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.InventoryItemConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.InventoryItemSpecificationConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.InventoryItemTransactionConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.OrderRecordConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.OrderConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductCategoryConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductSpecificationBlankConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.ProductSpecificationConsumers;
using InventoryIT.WebHost.Areas.Admin.RabbitMQ.WorkPlaceConsumers;

namespace InventoryIT.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(x =>
            {
                x.UseNpgsql(Configuration.GetConnectionString("InventoryDb"));
                x.UseLazyLoadingProxies();
            });

            var mapperConfiguration = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new InventoryMapper());
            });
            services.AddSingleton(mapperConfiguration.CreateMapper());

            #region Masstransit

            var massTransitSection = Configuration.GetSection("MassTransit");
            var url = massTransitSection.GetValue<string>("Url");
            var host = massTransitSection.GetValue<string>("Host");
            var userName = massTransitSection.GetValue<string>("UserName");
            var password = massTransitSection.GetValue<string>("Password");

            services.AddMassTransit(x =>
            {
                x.AddBus(busFactory =>
                {
                    var bus = Bus.Factory.CreateUsingRabbitMq(cfg =>
                    {
                        cfg.Host($"rabbitmq://{url}{host}", configurator =>
                        {
                            configurator.Username(userName);
                            configurator.Password(password);
                        });
                        cfg.ConfigureEndpoints(busFactory, KebabCaseEndpointNameFormatter.Instance);
                        cfg.UseJsonSerializer();
                        cfg.UseHealthCheck(busFactory);
                    });
                    return bus;
                });
                x.AddConsumer<CreateAmortisationReasonConsumer>();
                x.AddConsumer<GetAllAmortisationReasonConsumer>();
                x.AddConsumer<GetByIdAmortisationReasonConsumer>();
                x.AddConsumer<DeleteAmortisationReasonConsumer>();
                x.AddConsumer<UpdateAmortisationReasonConsumer>();
                
                x.AddConsumer<CreateAmortisationConsumer>();
                x.AddConsumer<GetAllAmortisationConsumer>();
                x.AddConsumer<GetByIdAmortisationConsumer>();
                x.AddConsumer<DeleteAmortisationConsumer>();
                x.AddConsumer<UpdateAmortisationConsumer>();
                
                x.AddConsumer<CreateDistributionRecordConsumer>();
                x.AddConsumer<GetAllDistributionRecordConsumer>();
                x.AddConsumer<GetByIdDistributionRecordConsumer>();
                x.AddConsumer<DeleteDistributionRecordConsumer>();
                x.AddConsumer<UpdateDistributionRecordConsumer>();
                
                x.AddConsumer<CreateDistributionConsumer>();
                x.AddConsumer<GetAllDistributionConsumer>();
                x.AddConsumer<GetByIdDistributionConsumer>();
                x.AddConsumer<DeleteDistributionConsumer>();
                x.AddConsumer<UpdateDistributionConsumer>();
                
                x.AddConsumer<CreateInventoryItemConsumer>();
                x.AddConsumer<GetAllInventoryItemConsumer>();
                x.AddConsumer<GetByIdInventoryItemConsumer>();
                x.AddConsumer<DeleteInventoryItemConsumer>();
                x.AddConsumer<UpdateInventoryItemConsumer>();
                
                x.AddConsumer<CreateInventoryItemSpecificationConsumer>();
                x.AddConsumer<GetAllInventoryItemSpecificationConsumer>();
                x.AddConsumer<GetByIdInventoryItemSpecificationConsumer>();
                x.AddConsumer<DeleteInventoryItemSpecificationConsumer>();
                x.AddConsumer<UpdateInventoryItemSpecificationConsumer>();
                
                x.AddConsumer<CreateInventoryItemTransactionConsumer>();
                x.AddConsumer<GetAllInventoryItemTransactionConsumer>();
                x.AddConsumer<GetByIdInventoryItemTransactionConsumer>();
                x.AddConsumer<DeleteInventoryItemTransactionConsumer>();
                x.AddConsumer<UpdateInventoryItemTransactionConsumer>();
                
                x.AddConsumer<CreateOrderRecordConsumer>();
                x.AddConsumer<GetAllOrderRecordConsumer>();
                x.AddConsumer<GetByIdOrderRecordConsumer>();
                x.AddConsumer<DeleteOrderRecordConsumer>();
                x.AddConsumer<UpdateOrderRecordConsumer>();
                
                x.AddConsumer<CreateOrderConsumer>();
                x.AddConsumer<GetAllOrderConsumer>();
                x.AddConsumer<GetByIdOrderConsumer>();
                x.AddConsumer<DeleteOrderConsumer>();
                x.AddConsumer<UpdateOrderConsumer>();
                
                x.AddConsumer<CreateProductCategoryConsumer>();
                x.AddConsumer<GetAllProductCategoryConsumer>();
                x.AddConsumer<GetByIdProductCategoryConsumer>();
                x.AddConsumer<DeleteProductCategoryConsumer>();
                x.AddConsumer<UpdateProductCategoryConsumer>();
                
                x.AddConsumer<CreateProductConsumer>();
                x.AddConsumer<GetAllProductConsumer>();
                x.AddConsumer<GetByIdProductConsumer>();
                x.AddConsumer<DeleteProductConsumer>();
                x.AddConsumer<UpdateProductConsumer>();
                
                x.AddConsumer<CreateProductSpecificationBlankConsumer>();
                x.AddConsumer<GetAllProductSpecificationBlankConsumer>();
                x.AddConsumer<GetByIdProductSpecificationBlankConsumer>();
                x.AddConsumer<DeleteProductSpecificationBlankConsumer>();
                x.AddConsumer<UpdateProductSpecificationBlankConsumer>();
                
                x.AddConsumer<CreateProductSpecificationConsumer>();
                x.AddConsumer<GetAllProductSpecificationConsumer>();
                x.AddConsumer<GetByIdProductSpecificationConsumer>();
                x.AddConsumer<DeleteProductSpecificationConsumer>();
                x.AddConsumer<UpdateProductSpecificationConsumer>();
                
                x.AddConsumer<CreateSpecificationBlankConsumer>();
                x.AddConsumer<GetAllSpecificationBlankConsumer>();
                x.AddConsumer<GetByIdSpecificationBlankConsumer>();
                x.AddConsumer<DeleteSpecificationBlankConsumer>();
                x.AddConsumer<UpdateSpecificationBlankConsumer>();
                
                x.AddConsumer<CreateSpecificationConsumer>();
                x.AddConsumer<GetAllSpecificationConsumer>();
                x.AddConsumer<GetByIdSpecificationConsumer>();
                x.AddConsumer<DeleteSpecificationConsumer>();
                x.AddConsumer<UpdateSpecificationConsumer>();
                
                x.AddConsumer<CreateWorkPlaceConsumer>();
                x.AddConsumer<GetAllWorkPlaceConsumer>();
                x.AddConsumer<GetByIdWorkPlaceConsumer>();
                x.AddConsumer<DeleteWorkPlaceConsumer>();
                x.AddConsumer<UpdateWorkPlaceConsumer>();

                //x.AddConsumer<BuildingByIdConsumer>(typeof(BuildingByIdConsumerDefinition));
            });

            services.AddMassTransitHostedService();
            #endregion

            #region DI

            services.AddTransient<IAmortisationService, AmortisationService>();
            services.AddTransient<IAmortisationRepository, AmortisationRepository>();
            
            services.AddTransient<IAmortisationReasonService, AmortisationReasonService>();
            services.AddTransient<IAmortisationReasonRepository, AmortisationReasonRepository>();
            
            services.AddTransient<IAmortisationRecordService, AmortisationRecordService>();
            services.AddTransient<IAmortisationRecordRepository, AmortisationRecordRepository>();
            
            services.AddTransient<IDistributionService, DistributionService>();
            services.AddTransient<IDistributionRepository, DistributionRepository>();
            
            services.AddTransient<IDistributionRecordService, DistributionRecordService>();
            services.AddTransient<IDistributionRecordRepository, DistributionRecordRepository>();
            
            services.AddTransient<IInventoryItemService, InventoryItemService>();
            services.AddTransient<IInventoryItemRepository, InventoryItemRepository>();
            
            services.AddTransient<IInventoryItemSpecificationService, InventoryItemSpecificationService>();
            services.AddTransient<IInventoryItemSpecificationRepository, InventoryItemSpecificationRepository>();
            
            services.AddTransient<IInventoryItemTransactionService, InventoryItemTransactionService>();
            services.AddTransient<IInventoryItemTransactionRepository, InventoryItemTransactionRepository>();
            
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            
            services.AddTransient<IOrderRecordService, OrderRecordService>();
            services.AddTransient<IOrderRecordRepository, OrderRecordRepository>();
            
            services.AddTransient<IProductCategoryService, ProductCategoryService>();
            services.AddTransient<IProductCategoryRepository, ProductCategoryRepository>();
            
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IProductRepository, ProductRepository>();
            
            services.AddTransient<IProductSpecificationBlankService, ProductSpecificationBlankService>();
            services.AddTransient<IProductSpecificationBlankRepository, ProductSpecificationBlankRepository>();
            
            services.AddTransient<ISpecificationBlankService, SpecificationBlankService>();
            services.AddTransient<ISpecificationBlankRepository, SpecificationBlankRepository>();
            
            services.AddTransient<ISpecificationService, SpecificationService>();
            services.AddTransient<ISpecificationRepository, SpecificationRepository>();
            
            services.AddTransient<IWorkPlaceService, WorkPlaceService>();
            services.AddTransient<IWorkplaceRepository, WorkplaceRepository>();
            


            #endregion  

            services.AddSwaggerGen(setupAction =>
            {
                setupAction.SwaggerDoc("v1", new OpenApiInfo { Title = "InventoryIT", Version = "v1" });
            });

            services.AddControllers();
        }

         public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(s =>
                {
                    s.SwaggerEndpoint("/swagger/v1/swagger.json", "InventoryIT v1");
                    s.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.List);
                    s.RoutePrefix = string.Empty;
                });
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
