﻿using InventoryIT.Core.Domain;
using System;
using System.Collections.Generic;

namespace InventoryIT.WebHost.Models
{
    public class AmortisationViewModel : BaseEntity
    {
        public InventoryItemTransactionViewModel InventoryItemTransaction { get; set; }
        public DateTime AmortisationDate { get; set; }
        public int ResponsibleEmployeeId { get; set; }
        public List<AmortisationRecordViewModel> AmortisationRecords { get; set; }
    }
}
