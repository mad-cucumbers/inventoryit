﻿using InventoryIT.Core.Domain;

namespace InventoryIT.WebHost.Models
{
    public class WorkplaceViewModel : BaseEntity
    {
        public int BuildingId { get; set; }
        public int RoomId { get; set; }
        public int DepartmentId { get; set; }
    }
}
