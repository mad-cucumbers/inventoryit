﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Enums;
using System;

namespace InventoryIT.WebHost.Models
{
    public class InventoryItemViewModel : BaseEntity
    {
        public string InventoryItemId { get; set; }
        public float Quantity { get; set; }
        public string ProductId { get; set; }
        public DateTime EntryDate { get; set; }
        public int ResponsibleEmployeeId { get; set; }
        public WorkplaceViewModel Workplace { get; set; }
        public InventoryItemStatus Status { get; set; }
        public InventoryItemSpecificationViewModel InventoryItemSpecification { get; set; }
    }
}
