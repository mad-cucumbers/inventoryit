﻿using InventoryIT.Core.Domain;

namespace InventoryIT.WebHost.Models
{
    public class OrderRecordViewModel : BaseEntity
    {
        public string OrderId { get; set; }
        public string ProductId { get; set; }
        public string InventoryItemId { get; set; }
        public float Quantity { get; set; }
        public string SerialNumber { get; set; }
    }
}
