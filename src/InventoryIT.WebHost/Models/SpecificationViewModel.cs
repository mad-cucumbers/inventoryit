﻿using InventoryIT.Core.Domain;
using InventoryIT.Core.Models.SpecificationModels;

namespace InventoryIT.WebHost.Models
{
    public class SpecificationViewModel : BaseEntity
    {
        public string SpecificationId { get; set; }
        public bool IsRequered { get; set; }
        public int SpecificationBlankId { get; set; }
    }
}
