﻿using InventoryIT.Core.Domain;
using System.Collections.Generic;

namespace InventoryIT.WebHost.Models
{
    public class ProductSpecificationBlankViewModel : BaseEntity
    {
        public string ProductSpecificationBlankId { get; set; }
        public string SpecificationBlankId { get; set; }
        public List<ProductSpecificationViewModel> ProductSpecifications { get; set; }
        public string ProductId { get; set; }
    }
}
