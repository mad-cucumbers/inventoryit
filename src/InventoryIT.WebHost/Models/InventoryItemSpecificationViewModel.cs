﻿using InventoryIT.Core.Domain;

namespace InventoryIT.WebHost.Models
{
    public class InventoryItemSpecificationViewModel : BaseEntity
    {
        public string SpecificationId { get; set; }
        public string InventoryItemId { get; set; }
        public string SpecificationValue { get; set; }
    }
}
