﻿using InventoryIT.Core.Domain;

namespace InventoryIT.WebHost.Models
{
    public class DistributionRecordViewModel : BaseEntity
    {
        public string DistributionId { get; set; }
        public string InventoryItemId { get; set; }
        public WorkplaceViewModel Workplace { get; set; }
        public float Quantity { get; set; }
    }
}
